version 1.0 (2016-09-12)
    * First public release, requires Infinispan 8.2+ and Java 8+.

version 1.1 (2016-09-12)
    * Abstracts common Infinispan store interfaces and classes into
      com.nimbusds:infinispan-cache-store-common:1.0.

version 1.2 (2016-09-17)
    * Extends SQLRecordTransformer interface with init() method.
    * Extends SQLRecordTransformer interface with getTableCreateStatement()
      method.

version 1.2.1 (2016-09-19)
    * Fixes PostgreSQL merge support, requires primary key constraint to be
      named "pk" (issue #1).

version 2.0 (2016-09-22)
    * Adds support for direct execution of SQL queries against the database.

version 2.1 (2016-09-28)
    * Adds SQL field transformation utility for Java collections, strings and
      time instances.
    * Introduces automatic detection of the primary key constraint name on SQL
      merge statements in PostgreSQL.

version 2.2 (2016-10-17)
    * Implements custom XML sql-store configuration elements (issue #3).
    * Renames the Maven artifactId to infinispan-cachestore-sql to match
      Infinispan naming conversion (issue #2).

version 2.2.1 (2016-10-17)
    * Fixes infinispan-cachestore-sql-config-2.2.xsd version.

version 2.3 (2016-10-19)
    * Generates default poolName based on [cacheName].sqlStore pattern. Can be
      overridden via the matching configuration property (issue #4).

version 2.3.1 (2016-10-19)
    * Fixes query-executor XML attribute name (issue #5).

version 2.4 (2016-10-19)
    * Enables Dropwizard Metrics.

version 2.4.1 (2016-10-19)
    * Wraps reusable jOOQ DSLContext around DataSource (issue #6).

version 2.5 (2016-10-24)
    * Add HikariCP health checks to Dropwizard.

version 2.5.1 (2016-10-24)
    * Fixes Dropwizard metrics and health check registration with multiple
      SQLStore instances (iss #8).

version 2.5.2 (2016-10-27)
    * Logs Infinispan expiration thread wake up interval for cache on SQL store
      init.

version 2.5.3 (2016-10-27)
    * Fixes "shared" and other inherited top-level XML attribute parsing.

version 2.5.4 (2016-10-27)
    * Closes Hikari connection pool on SQLStore stop.

version 2.5.5 (2016-10-27)
    * Sets H2 rendering style to RenderNameStyle.AS_IS.

version 2.5.6 (2016-11-14)
    * Upgrades to com.nimbusds:infinispan-cachestore-common:1.3.

version 2.5.7 (2016-12-09)
    * Brackets Infinispan dependency to prevent conflict with buggy 9.0.0Beta1
      release.

version 2.5.8 (2017-05-25)
    * Makes jOOQ 3.9.0 the minimal dependency.
    * Makes HikariCP 2.6.0 the minimal dependency.
    * Switches to MariaDB JDBC driver for testing to avoid GPL dependencies.
      The MariaDB JDBC driver is MySQL compliant, comes with more permissive
      LGPL license.

version 2.6 (2017-06-26)
    * Adds new optional boolean "create-table-if-missing" configuration
      attribute to the "sql-store" XML element. If set to "true" during
      initialisation the SQLStore will execute an SQL "CREATE TABLE IF NOT
      EXISTS" statement supplied by the configured SQLRecordTransformer. If set to
      "false" execution of this statement will be skipped. The default setting
      is "true". "CREATE TABLE IF NOT EXISTS" can be disabled if the database
      permissions granted to the SQLStore user don't include the SQL "CREATE"
      query.
    * Removes redundant JSON Smart dependency.

version 2.6.1 (2017-06-26)
    * Updates the XML schema definition (XSD) to include the new optional
      "create-table-if-missing" configuration setting.

version 2.6.2 (2017-07-01)
    * Restores older version 2.2 XML configuration schema and parser.

version 2.6.3 (2017-09-15)
    * Logs dataSourceClassName and dataSource.url HikariCP configuration
      properties at startup if set (issue #11).
    * Makes tests local timezone aware.

version 2.7 (2017-12-26)
    * Adds SQLStore.getSQLRecordTransformer method.

version 2.8 (2018-04-15)
    * New SQLTableTransformer interface to facilitate table add, modify and
      drop column changes when the SQL store is started.
    * New Hikari CP metrics *.sqlStore.pool.MinConnections and
      *.sqlStore.pool.MaxConnections.

version 2.9 (2018-04-25)
    * Upgrades to org.infinispan:infinispan-core:9.0.0Final+
    * Upgrades to com.nimbusds:infinispan-cachestore-common:1.4+
    * Upgrades to com.nimbusds:common:2.27+
    * Upgrades to io.dropwizard.metrics:metrics-core:3.1.2+

version 2.9.1 (2018-04-27)
    * Adds @Store(shared = true) annotation.

version 2.9.2 (2018-04-27)
    * Removes old SQLStoreConfigurationParser22.

version 3.0 (2018-05-25)
    * Implements AdvancedCacheExpirationWriter which passing the entire purged
      Infinispan entry to the listeners, not just the key.

version 3.0.1 (2018-05-27)
    * Fixes concurrency bug when logging number of expired entries.

version 3.0.2 (2018-05-27)
    * Refactors the ExpiredEntryReaper.

version 3.0.3 (2018-07-11)
    * Bumps minimal jOOQ dependency to 3.11.0

version 3.1 (2019-03-07)
    * Don't load and process expired entries.
    * Implements SQLStore.publishEntries.
    * Adds SQLTimers "[prefix]sqlStore.loadTimer",
      "[prefix]sqlStore.writeTimer", "[prefix]sqlStore.deleteTimer",
      "[prefix]sqlStore.processTimer" and "[prefix]sqlStore.purgeTimer".

version 3.1.1 (2019-03-07)
    * Simplifies publishEntries code.

version 3.1.2 (2019-03-29)
    * Bumps max Infinispan dependency to 9.4.99.Final inclusive.

version 3.1.3 (2019-07-02)
    * Factors out SQLFieldTransformer.parseStringArrayFromJSONArrayString,
      wraps ArrayStoreException in PersistenceException.

version 3.1.4 (2019-10-09)
    * Bumps dependencies.

version 3.1.5 (2019-10-10)
    * Bumps dependencies.

version 3.1.6 (2019-11-06)
    * Upgrades to jOOQ 3.12.3.
    * Prevents NPE on SQLStore.stop with data source not initialised.

version 4.0 (2019-11-06)
    * Requires Java 11+.

version 4.0.1 (2019-11-06)
    * Updates SQLFieldTransformer to support MS SQL Server.

version 4.0.2 (2019-11-11)
    * Makes MS SQL Server 2016 the minimum version.

version 4.0.3 (2019-11-11)
    * Restores SQLDialect.POSTGRES_9_5.
    * Updates SQLFieldTransformer to support all PostgreSQL versions.

version 4.0.4 (2019-11-14)
    * Adds CustomSQLDataTypes.SQLSERVER_NVARCHAR for Microsoft SQL Server
      NVARCHAR data type for 'N' prefixing of inline string values.

version 4.0.5 (2019-11-14)
    * Puts null value check in SQLServerNVarcharBinding.

version 4.0.6 (2019-11-16)
    * Makes SQLServerNVarcharBinding.sql safer.

version 4.1 (2019-12-25)
    * Makes SQLTableUtils class public.

version 4.1.1 (2019-12-25)
    * Makes SQLTableUtils.getColumnNames class public.

version 4.2 (2020-05-06)
    * Adds new connection-pool XML attribute for specifying the name of another
      Infinispan cache to use its SQL store Hikari connection pool.

version 4.2.1 (2020-05-07)
    * Fixes startup deferral of SQL stores relying on other connection pool.

version 4.2.2 (2020-05-07)
    * Refines log messages for shared connection pool usage.

version 4.2.3 (2020-12-18)
    * SQLFieldTransformer.parseStringArrayFromJSONArrayString includes illegal
      string in thrown PersistenceException.
    * SQLStore.load logs exceptions thrown by
      SQLRecordTransformer.toInfinispanEntry under IS0137.

version 4.2.4 (2021-04-12)
    * Updates dependencies.

version 4.2.5 (2021-04-18)
    * Bumps and bounds JSON Smart.

version 4.2.6 (2021-10-24)
    * Bumps dependencies.

version 4.2.7 (2022-08-12)
    * Bumps dependencies.

version 4.2.8 (2022-09-14)
    * Updates to com.zaxxer:HikariCP:5.0.1
    * Updates DropWizard to 4.2.12
    * Bumps JDBC test dependencies.

version 5.0 (2022-10-31)
    * Upgrades to jOOQ 3.17.4
    * Upgrades to H2 2.1.x
    * Updates Log4j to 2.19.0

version 6.0 (2022-11-12)
    * Upgrades to Infinispan 14.0.

version 7.0 (2023-02-25)
    * Modifies the SQLRecordTransformer interface to work with an
      RetrievedSQLRecord interface (breaking change).
    * Adds create-table-ignore-errors XML config attribute.
    * Adds Oracle database support.
    * Adds IS0136 log info message for about to create table events.
    * Updates the IS0129 log info message to include the number of changed rows
      after a create table execution.
    * Updates the IS0103 log warn / fatal message on table create failure to
      include the create-table-ignore-errors configuration setting.
    * Changes the IS0133 log level from warn to info for found table
      transformer.
    * Updates the IS0134 log info message to include the cache name for a
      successfully transformed table.
    * Updates to Infinispan 14.0.6.
    * Updates to Infinispan Cache Store Common 3.0.1.
    * Updates DropWizard to 4.2.15.

version 7.0.1 (2023-02-25)
    * Fixes NPE in SQLStore.write with Metadata is null.

version 7.0.2 (2023-03-04)
    * Adds Oracle CLOB write support.

version 7.0.3 (2023-07-16)
    * JOOQFixes.concatChunksForOracleWriteClob must escape quote chars (iss #20).

version 7.0.4 (2023-07-16)
    * Updates JSON Smart to 2.4.10
    * Updates Guava to 32.0.0-jre
    * Updates the test PostgreSQL JDBC driver to 42.5.1

version 7.0.5 (2023-09-24)
    * Updates to Infinispan 14.0.17.
    * Updates to NimbusDS Common 2.51.
    * Updates the test PostgreSQL JDBC driver to 42.5.4.
    * Updates the test SQL Server JDBC driver to 12.2.0.jre11.
    * Updates the test Oracle Database JDBC driver to 21.9.0.0.
    * Updates to Log4j 2.20.0.
    * Updates DropWizard to 4.2.17.

version 7.0.6 (2023-10-19)
    * Removes HikariCP properties with empty or blank values.
    * Updates the MariaDB JDBC driver to 2.7.10.

version 7.1 (2023-11-01)
    * Adds new SQLRecordTransformer default getKeyColumnsForExpiredEntryReaper
      and getKeyValuesForExpiredEntryReaper methods to enable memory efficient
      expired entry reaping with paged retrieval (see below). Recommended for
      Infinispan maps / caches with many entries (100K+) subject to expiration.
    * Updates the ExpiredEntryReaper to conserve memory by interleaving the
      expired entry deletion in batches of up to 100 entries.
    * Introduces a new alternative ExpiredEntryPagedReaper to further conserve
      memory by using paging of 100 entries with key set seek, with the expired
      entry deletion taking place in batches between the pages. Requires a
      SQLRecordTransformer that implements the new
      getKeyColumnsForExpiredEntryReaper and getKeyValuesForExpiredEntryReaper
      methods (iss #21).
    * SQLTableTransformer.getTransformTableStatements must receive the column
      names normalised to lowercase. Intended for Oracle Database deployments
      (iss #24).
    * Optimises SQLTableUtils.getColumnNames for Oracle Database deployments.
      LIMIT 0 excessively slow on tables with many records, switch to LIMIT 1
      (iss #22).
    * The ExpiredEntryReaper and ExpiredEntryPagedReaper thread must not be
      caused to terminate when an unchecked exception is thrown by the
      SQLRecordTransformer.toSQLRecord method. Instead, the exception must be
      swallowed and an error with the offending SQL record logged (iss #23).
    * Updates to Infinispan 14.0.19.
    * Updates to NimbusDS Common 2.52.
    * Updates the MariaDB JDBC driver to 2.7.10.
    * Updates DropWizard to 4.2.19.
    * Updates to Log4j 2.21.1.

version 7.1.1 (2023-11-08)
    * The *.sqlStore.deleteTimer must cover SQL delete queries performed by the
      ExpiredEntryReaper (iss #26).
    * ExpiredEntryPagedReaper.purgeExtended: The delete list must be cleared
      after a page iteration with detected expired entries (iss #25).

version 7.2 (2023-11-18)
    * Updates the urn:infinispan:config:store:sql schema to v3.2.
    * Adds new optional expired-query-page-limit XML attribute with a default
      value of 250. Used to set the page limit in SQL queries to select expired
      records.
    * Increases the (now default) page limit in SQL queries to select expired
      record from 100 to 250 records.
    * Inlines the page limit in ExpiredEntryPagedReaper (iss #29).
    * ExpiredEntryReaper and ExpiredEntryPagedReaper should get the current
      time close to the entry expiration evaluation (iss #30).

version 7.3 (2023-11-18)
    * Adds SQLRecordTransformer default getExpiredCondition method to return
      an SQL WHERE condition to select only expired records from the table.
      When such a condition is available it will be used by the
      ExpiredEntryReaper and the ExpiredEntryPagedReaper.

version 7.3.1 (2023-11-18)
    * Bumps HikariCP to 5.1.0

version 7.4 (2023-11-19)
    * SQLRecordTransformer.getExpiredCondition receives a long now argument.
    * The ExpiredEntryReaper.delete method switches to a Queue argument.

version 7.4.1 (2023-11-20)
    * Purge exceptions must be swallowed and an error logged (iss #32).
    * Individual purge delete exceptions must be swallowed and an error logged
      (iss #31).
    * Refactors the ExpiredEntryReaper and ExpiredEntryPagedReaper.

version 7.4.2 (2023-12-07)
    * SQLStore.write must not serialise the jOOQ Query to an intermediate
      String unless when dealing with Oracle large (N)CLOB chunking. By using
      direct Query execution a PreparedStatement can be correctly inferred (iss
      #35).
    * Updates SQLStore.write to switch from the deprecated jOOQ mergeInto() to
      an insertInto() for PostgreSQL and Oracle databases (iss #34).
    * Updates to org.jooq.pro-java-11:jooq:3.18.7

version 7.4.3 (2023-12-07)
    * Factors out StringToStringConverter from SQLServerNVarcharBinding.
    * Adds test with jOOQ JSON.json for PostgreSQL.

version 8.0 (2023-12-10)
    * Java 17
    * Removes unused PostgreSQL primary key statement fix from JOOQFixes.
    * Updates to net.minidev:json-smart:2.5.0
    * Updates to com.nimbusds:common:3.0.1
    * Updates to DropWizard metrics 4.2.23
    * Updates to org.apache.logging.log4j:log4j-api:2.22.0

version 8.1 (2023-12-26)
    * Adds SQLFieldTransformer.toUpperCaseString method.
    * Updates to org.mariadb.jdbc:mariadb-java-client:2.7.11

version 8.1.1 (2023-12-30)
    * ExpiredEntryPagedReaper must log the IS0128 debug message in all
      execution paths (iss #37).

version 8.1.2 (2024-03-04)
    * Updates to Infinispan 14.0.24.Final
    * Updates to com.nimbusds:infinispan-cachestore-common:4.0
    * Updates to com.google.guava:guava:32.1.3-jre
    * Updates to Dropwizard Metric 4.2.25
    * Updates to Log4j 2.22.1
    * Updates to org.kohsuke.metainf-services:metainf-services:1.11
    * Updates to com.h2database:h2:2.2.224
    * Updates to org.postgresql:postgresql:42.7.2
    * Updates to org.mariadb.jdbc:mariadb-java-client:2.7.12
    * Updates to com.microsoft.sqlserver:mssql-jdbc:12.6.1.jre11
    * Updates to com.oracle.database.jdbc:ojdbc11:21.13.0.0

version 8.2 (2024-03-04)
    * Logs the transaction isolation level at startup (IS0143) (iss #38).
    * Adds org.apache.logging.log4j:log4j-slf4j-impl:2.22.1 test dependency
