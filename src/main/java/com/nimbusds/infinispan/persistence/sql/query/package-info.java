/**
 * SQL query executor interfaces.
 */
package com.nimbusds.infinispan.persistence.sql.query;