package com.nimbusds.infinispan.persistence.sql.transformers;


import net.jcip.annotations.Immutable;
import org.jooq.Converter;


/**
 * String-to-string converter for data type values.
 */
@Immutable
public final class StringToStringConverter implements Converter<String, String> {


        @Override
        public String from(final String databaseObject) {
                return databaseObject;
        }


        @Override
        public String to(final String userObject) {
                return userObject;
        }


        @Override
        public Class<String> fromType() {
                return String.class;
        }


        @Override
        public Class<String> toType() {
                return String.class;
        }
}
