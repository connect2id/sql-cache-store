package com.nimbusds.infinispan.persistence.sql;


import com.zaxxer.hikari.HikariDataSource;
import org.infinispan.persistence.spi.PersistenceException;

import java.sql.Connection;


/**
 * Transaction isolation utility.
 */
class TXIsolation {


        /**
         * Inspects the transaction isolation level of connections in the
         * specified Hikari connection pool.
         *
         * @param dataSource The Hikari connection pool.
         *
         * @return The isolation level string identifier.
         */
        static String inspect(final HikariDataSource dataSource) {

                try (Connection connection = dataSource.getConnection()) {

                        int txIsolationConst = connection.getTransactionIsolation();

                        switch (txIsolationConst) {
                                case Connection.TRANSACTION_NONE -> {
                                        return "NONE";
                                }
                                case Connection.TRANSACTION_READ_UNCOMMITTED -> {
                                        return "TRANSACTION_READ_UNCOMMITTED";
                                }
                                case Connection.TRANSACTION_READ_COMMITTED -> {
                                        return "TRANSACTION_READ_COMMITTED";
                                }
                                case Connection.TRANSACTION_REPEATABLE_READ -> {
                                        return "TRANSACTION_REPEATABLE_READ";
                                }
                                case Connection.TRANSACTION_SERIALIZABLE -> {
                                        return "TRANSACTION_SERIALIZABLE";
                                }
                                default -> {
                                        return "UNKNOWN";
                                }
                        }

                } catch (Exception e) {
                        throw new PersistenceException(e);
                }
        }


        private TXIsolation() {
        }
}
