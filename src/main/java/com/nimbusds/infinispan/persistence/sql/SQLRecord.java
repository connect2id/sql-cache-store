package com.nimbusds.infinispan.persistence.sql;


import java.util.List;
import java.util.Map;

import org.jooq.Field;


/**
 * SQL record for insert / update.
 */
public interface SQLRecord {
	
	
	/**
	 * Returns the column names that serve as keys for the SQL record. Must
	 * be included in the {@link #getFields() returned fields}.
	 *
	 * @return The key column names, one or more. Never empty.
	 */
	List<Field<?>> getKeyColumns();
	
	
	/**
	 * Returns the fields for the SQL record.
	 *
	 * @return The fields. Never empty.
	 */
	Map<Field<?>,?> getFields();
}
