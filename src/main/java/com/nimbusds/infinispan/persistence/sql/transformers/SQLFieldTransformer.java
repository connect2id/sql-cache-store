package com.nimbusds.infinispan.persistence.sql.transformers;


import com.nimbusds.infinispan.persistence.sql.RetrievedSQLRecord;
import net.jcip.annotations.ThreadSafe;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import net.minidev.json.parser.ParseException;
import org.infinispan.persistence.spi.PersistenceException;
import org.jooq.SQLDialect;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;


/**
 * SQL field transformer.
 */
@ThreadSafe
public class SQLFieldTransformer {
	
	
	/**
	 * The preferred collection data type.
	 */
	private final CollectionDataType collectionDataType;
	
	
	/**
	 * Creates a new SQL field transformer.
	 *
	 * @param sqlDialect The SQL dialect. Must be for H2, MySQL,
	 *                   PostgreSQL 9.5+ or MS SQL Server 2016. Not
	 *                   {@code null}.
	 */
	public SQLFieldTransformer(final SQLDialect sqlDialect) {
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			collectionDataType = CollectionDataType.ARRAY;
		} else if (SQLDialect.MYSQL.equals(sqlDialect)) {
			collectionDataType = CollectionDataType.JSON;
		} else if (SQLDialect.POSTGRES.family().equals(sqlDialect.family())) {
			collectionDataType = CollectionDataType.ARRAY;
		} else if (SQLDialect.SQLSERVER2016.equals(sqlDialect)) {
			collectionDataType = CollectionDataType.JSON;
		} else if (SQLDialect.ORACLE.equals(sqlDialect)) {
			collectionDataType = CollectionDataType.JSON;
		} else {
			throw new IllegalArgumentException("Unsupported SQL dialect: " + sqlDialect);
		}
	}
	
	
	/**
	 * Returns the string representation of the specified object.
	 *
	 * @param o The object, {@code null} if not specified.
	 *
	 * @return The string representation, {@code null} if not specified.
	 */
	public static String toString(final Object o) {
		
		if (o == null) return null;
		
		return o.toString();
	}


	/**
	 * Returns the upper case string representation of the specified
	 * object.
	 *
	 * @param o The object, {@code null} if not specified.
	 *
	 * @return The upper case string representation, {@code null} if not
	 *         specified.
	 */
	public static String toUpperCaseString(final Object o) {

		String s = toString(o);

		return s != null ? s.toUpperCase() : null;
	}
	
	
	/**
	 * Returns the appropriate SQL representation of the specified
	 * collection.
	 *
	 * @param collection The collection, {@code null} if not specified.
	 *
	 * @return The SQL collection representation, {@code null} if not
	 *         specified.
	 */
	public Object toSQLCollection(final Collection<?> collection) {
		
		if (collection == null || collection.isEmpty()) {
			return null;
		}
		
		if (CollectionDataType.ARRAY.equals(collectionDataType)) {
			
			String[] out = new String[collection.size()];
			
			int i = 0;
			for (Object item: collection) {
				out[i++] = item.toString();
			}
			
			return out;
			
		} else {
			// serialise to JSON array
			var jsonArray = new JSONArray();
			for (Object item: collection) {
				jsonArray.add(item.toString());
			}
			return jsonArray.toJSONString();
		}
	}
	
	
	/**
	 * Returns the string representation of the specified JSON object.
	 *
	 * @param jsonObject The JSON object, {@code null} if not specified.
	 *
	 * @return The JSON object string representation, {@code null} if not
	 *         specified.
	 */
	public static String toSQLString(final JSONObject jsonObject) {
		
		if (jsonObject == null || jsonObject.isEmpty()) {
			return null;
		}
		
		return jsonObject.toJSONString();
	}
	
	
	/**
	 * Returns an SQL timestamp representation of the specified instant.
	 *
	 * @param instant The instant, {@code null} if not specified.
	 *
	 * @return The SQL timestamp, {@code null} if not specified.
	 */
	public static Timestamp toTimestamp(final Instant instant) {
		if (instant == null) {
			return null;
		}
		
		return new Timestamp(instant.toEpochMilli());
	}
	
	
	/**
	 * Parses a string array from the specified SQL record field.
	 *
	 * @param fieldName The SQL field name. Must not be {@code null}.
	 * @param sqlRecord The SQL record. Must not be {@code null}.
	 *
	 * @return The string array, {@code null} if not specified.
	 */
	public String[] parseSQLStringCollection(final String fieldName, final RetrievedSQLRecord sqlRecord) {
		
		if (CollectionDataType.ARRAY.equals(collectionDataType)) {
			
			// Expect SQL array
			return sqlRecord.get(fieldName, String[].class);
			
		} else {
			// Expect JSON array string
			return parseStringArrayFromJSONArrayString(sqlRecord.get(fieldName, String.class));
		}
	}
	
	
	/**
	 * Parses a string array from the specified JSON array string.
	 *
	 * @param s The JSON array string, {@code null} if none.
	 *
	 * @return The string array, {@code null} if none.
	 */
	static String[] parseStringArrayFromJSONArrayString(final String s) {
		
		if (s == null) return null;
		
		JSONArray jsonArray;
		try {
			jsonArray = (JSONArray) JSONValue.parseStrict(s);
		} catch (ParseException e) {
			throw new PersistenceException("Couldn't parse JSON array: " + e.getMessage() + ": " + s, e);
		}
		
		String[] out = new String[jsonArray.size()];
		for (int i=0; i < out.length; i++) {
			Object item = jsonArray.get(i);
			if (item instanceof String) {
				out[i] = (String)item;
			} else {
				throw new PersistenceException("Couldn't parse JSON array of strings: Item " + i + " not a string: " + s);
			}
		}
		return out;
	}
	
	
	/**
	 * Parses a JSON object from the specified string.
	 *
	 * @param jsonObjectString The JSON object string, {@code null} if not
	 *                         specified.
	 *
	 * @return The JSON object, {@code null} if not specified.
	 */
	public static JSONObject parseJSONObject(final String jsonObjectString) {
		
		if (jsonObjectString == null) return null;
		
		try {
			return (JSONObject) JSONValue.parseStrict(jsonObjectString);
		} catch (ParseException | ClassCastException e) {
			throw new PersistenceException("Couldn't parse JSON object: " + e.getMessage(), e);
		}
	}
	
	
	/**
	 * Parses a time instant from the specified SQL timestamp.
	 *
	 * @param timestamp The SQL timestamp, {@code null} if not specified.
	 *
	 * @return The time instant, {@code null} if not specified.
	 */
	public static Instant parseInstant(final Timestamp timestamp) {
		
		if (timestamp == null) return null;
		
		return Instant.ofEpochMilli(timestamp.getTime());
	}
}
