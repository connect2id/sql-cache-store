package com.nimbusds.infinispan.persistence.sql;


import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.Table;
import org.jooq.impl.DSL;

import java.util.LinkedList;
import java.util.List;


/**
 * SQL table utilities.
 */
public class SQLTableUtils {
	
	
	/**
	 * Gets the column names of the specified table.
	 *
	 * @param table      The table.
	 * @param dslContext The DSL context.
	 *
	 * @return The column names normalised to lowercase, empty list if
	 *         none.
	 */
	public static List<String> getColumnNames(final Table<?> table, final DSLContext dslContext) {

		// LIMIT 0 causes extremely slow execution in Oracle DB with many records, see iss #22
		Result<?> result = dslContext.selectFrom(table).limit(DSL.inline(1)).fetch();
		
		List<String> columnNames = new LinkedList<>();
		
		for (int i=0; i < result.fields().length; i++)
			columnNames.add(result.fields()[i].getName().toLowerCase());
		
		return columnNames;
	}
	
	
	private SQLTableUtils(){}
}
