package com.nimbusds.infinispan.persistence.sql.transformers;


import org.jooq.*;
import org.jooq.conf.ParamType;
import org.jooq.impl.DSL;

import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Types;


/**
 * Microsoft SQL server NVARCHAR binding, prefixed 'N' to inline string values.
 *
 * <p>Used https://github.com/jOOQ/jOOQ/issues/7825
 */
public class SQLServerNVarcharBinding implements Binding<String,String> {
	
	
	@Override
	public Converter<String, String> converter() {
		return new StringToStringConverter();
	}
	
	
	@Override
	public void sql(final BindingSQLContext<String> ctx) {
		if (ctx.render().paramType() == ParamType.INLINED) {
			// Prefix value with 'N' for UTF16
			// https://github.com/jOOQ/jOOQ/issues/9548
			ctx.render().sql((ctx.value() != null ? "N" : "")).visit(DSL.inline(ctx.value()));
		} else {
			ctx.render().sql("?");
		}
	}
	
	
	// Registering NVARCHAR types for JDBC CallableStatement OUT parameters
	@Override
	public void register(final BindingRegisterContext<String> ctx) throws SQLException {
		ctx.statement().registerOutParameter(ctx.index(), Types.NVARCHAR);
	}
	
	
	// Converting the String value and setting that on a JDBC PreparedStatement
	@Override
	public void set(final BindingSetStatementContext<String> ctx) throws SQLException {
		ctx.statement().setNString(ctx.index(), ctx.value());
	}
	
	
	// Getting a String value from a JDBC ResultSet and converting that to a JsonElement
	@Override
	public void get(final BindingGetResultSetContext<String> ctx) throws SQLException {
		ctx.convert(converter()).value(ctx.resultSet().getNString(ctx.index()));
	}
	
	
	// Getting a String value from a JDBC CallableStatement
	@Override
	public void get(final BindingGetStatementContext<String> ctx) throws SQLException {
		ctx.convert(converter()).value(ctx.statement().getNString(ctx.index()));
	}
	
	
	// Getting a value from a JDBC SQLInput (useful for Oracle OBJECT types)
	@Override
	public void get(final BindingGetSQLInputContext<String> ctx) throws SQLException {
		throw new SQLFeatureNotSupportedException();
	}
	
	
	// Setting a value on a JDBC SQLOutput (useful for Oracle OBJECT types)
	@Override
	public void set(final BindingSetSQLOutputContext<String> ctx) throws SQLException {
		throw new SQLFeatureNotSupportedException();
	}
}
