package com.nimbusds.infinispan.persistence.sql;


/**
 * Retrieved SQL record.
 */
public interface RetrievedSQLRecord {
	
	
	/**
	 * Gets a value from the record.
	 *
	 * @param fieldName The field name.
	 *
	 * @return The value of the field contained in the record.
	 */
	Object get(String fieldName);
	
	
	/**
	 * Gets a converted value from the record.
	 *
	 * @param fieldName The field name.
	 * @param type      The conversion type
	 * @param <U>       The conversion type parameter
	 *
	 * @return The value of the field contained in the record.
	 */
	<U> U get(String fieldName, Class<? extends U> type);
	
	
	/**
	 * Gets the number of fields in the record.
	 *
	 * @return The number of fields in the record.
	 */
	int size();
}
