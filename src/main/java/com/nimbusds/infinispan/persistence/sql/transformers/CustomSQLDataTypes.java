package com.nimbusds.infinispan.persistence.sql.transformers;


import org.jooq.DataType;
import org.jooq.impl.SQLDataType;


/**
 * Custom SQL data types for use in jOOQ and
 * {@link com.nimbusds.infinispan.persistence.sql.SQLRecordTransformer}s.
 */
public class CustomSQLDataTypes {
	
	
	/**
	 * Microsoft SQL Server NVARCHAR data type for 'N' prefixing of inline
	 * string values.
	 */
	public static final DataType<String> SQLSERVER_NVARCHAR = SQLDataType.NVARCHAR.asConvertedDataType(new SQLServerNVarcharBinding());
}
