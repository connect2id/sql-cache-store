package com.nimbusds.infinispan.persistence.sql.query;


import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;


/**
 * SQL query executor.
 */
public interface SQLQueryExecutor<K,V> extends QueryExecutor<K,V> {
	
	
	/**
	 * Initialises the SQL query executor.
	 *
	 * @param initCtx The initialisation context. Not {@code null}.
	 */
	void init(final SQLQueryExecutorInitContext<K,V> initCtx);
}
