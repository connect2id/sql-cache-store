package com.nimbusds.infinispan.persistence.sql;


import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.jooq.impl.DSL.field;

import net.jcip.annotations.Immutable;
import org.jooq.Field;


/**
 * Immutable SQL record.
 */
@Immutable
public final class ImmutableSQLRecord implements SQLRecord {
	

	/**
	 * The key column names.
	 */
	private final List<Field<?>> keyCols;
	
	
	/**
	 * The fields.
	 */
	private final Map<Field<?>,?> fields;
	

	/**
	 * Creates a new immutable SQL record.
	 *
	 * @param keyCols The key column names. Must not be empty.
	 * @param fields  The fields. Must not be empty.
	 */
	public ImmutableSQLRecord(final List<Field<?>> keyCols, final Map<Field<?>, ?> fields) {
		assert ! keyCols.isEmpty();
		this.keyCols = keyCols;
		assert ! fields.isEmpty();
		this.fields = fields;
	}
	
	
	/**
	 * Creates a new immutable SQL record with a single key column.
	 *
	 * @param keyCol The key column name. Must not be {@code null}.
	 * @param fields The fields. Must not be empty.
	 */
	public ImmutableSQLRecord(final String keyCol, final Map<Field<?>, ?> fields) {
		this(Collections.singletonList(field(keyCol)), fields);
	}
	
	
	@Override
	public List<Field<?>> getKeyColumns() {
		return keyCols;
	}
	
	
	@Override
	public Map<Field<?>,?> getFields() {
		return Collections.unmodifiableMap(fields);
	}
}
