package com.nimbusds.infinispan.persistence.sql.config;


import java.util.HashMap;
import java.util.Map;


/**
 * SQL store XML configuration attributes.
 */
public enum Attribute {
	
	
	/**
	 * Unknown XML attribute.
	 */
	UNKNOWN(null), // must be first
	
	
	/**
	 * The XML attribute for the SQL record transformer.
	 */
	RECORD_TRANSFORMER("record-transformer"),
	
	
	/**
	 * The XML attribute for the SQL query executor.
	 */
	QUERY_EXECUTOR("query-executor"),
	
	
	/**
	 * The XML attribute for the SQL dialect.
	 */
	SQL_DIALECT("sql-dialect"),
	
	
	/**
	 * The optional XML attribute for the create-table-if-missing setting.
	 */
	CREATE_TABLE_IF_MISSING("create-table-if-missing"),
	
	
	/**
	 * The optional XML attribute for the create-table-ignore errors
	 * setting.
	 */
	CREATE_TABLE_IGNORE_ERRORS("create-table-ignore-errors"),
	
	
	/**
	 * The optional XML attribute for the connection pool reference.
	 */
	CONNECTION_POOL("connection-pool"),


	/**
	 * The optional XML attribute for the page limit in SQL queries to
	 * select expired records.
	 */
	EXPIRED_QUERY_PAGE_LIMIT("expired-query-page-limit");
	
	
	/**
	 * The attribute name.
	 */
	private final String name;
	
	
	/**
	 * Creates a new attribute with the specified name.
	 *
	 * @param name The attribute name.
	 */
	Attribute(final String name) {
		this.name = name;
	}
	
	
	/**
	 * Gets the local name of this attribute.
	 *
	 * @return The local name.
	 */
	public String getLocalName() {
		return name;
	}
	
	
	/**
	 * The enumerated attributes as map.
	 */
	private static final Map<String, Attribute> attributes;
	
	static {
		final Map<String, Attribute> map = new HashMap<>();
		for (Attribute attribute : values()) {
			final String name = attribute.getLocalName();
			if (name != null) {
				map.put(name, attribute);
			}
		}
		attributes = map;
	}
	
	
	/**
	 * Returns the matching attribute for the specified local name.
	 *
	 * @param localName The local name.
	 *
	 * @return The attribute.
	 */
	public static Attribute forName(final String localName) {
		final Attribute attribute = attributes.get(localName);
		return attribute == null ? UNKNOWN : attribute;
	}
}
