package com.nimbusds.infinispan.persistence.sql.config;


import java.util.HashMap;
import java.util.Map;


/**
 * SQL store XML configuration attributes.
 */
public enum Element {
	
	
	/**
	 * Unknown XML element.
	 */
	UNKNOWN(null), // must be first
	
	
	/**
	 * SQL store element.
	 */
	SQL_STORE("sql-store");
	
	
	/**
	 * The element name.
	 */
	private final String name;
	
	
	/**
	 * Creates a new element with the specified name.
	 *
	 * @param name The element name.
	 */
	Element(final String name) {
		this.name = name;
	}
	
	
	/**
	 * Gets the local name of this element.
	 *
	 * @return The local name.
	 */
	public String getLocalName() {
		return name;
	}
	
	
	/**
	 * The enumerated attributes as map.
	 */
	private static final Map<String, Element> MAP;
	
	static {
		final Map<String, Element> map = new HashMap<>();
		for (Element element : values()) {
			final String name = element.getLocalName();
			if (name != null) {
				map.put(name, element);
			}
		}
		MAP = map;
	}
	
	
	/**
	 * Returns the matching elements for the specified local name.
	 *
	 * @param localName The local name.
	 *
	 * @return The element.
	 */
	public static Element forName(final String localName) {
		final Element element = MAP.get(localName);
		return element == null ? UNKNOWN : element;
	}
}
