package com.nimbusds.infinispan.persistence.sql.config;


import org.infinispan.commons.CacheConfigurationException;
import org.infinispan.commons.configuration.io.ConfigurationReader;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.infinispan.configuration.parsing.*;
import org.jooq.SQLDialect;
import org.kohsuke.MetaInfServices;

import static org.infinispan.commons.util.StringPropertyReplacer.replaceProperties;


/**
 * XML configuration parser.
 */
@MetaInfServices
@Namespaces({
	@Namespace(uri = "urn:infinispan:config:store:sql:3.2", root = "sql-store"),
	@Namespace(root = "sql-store")
})
public class SQLStoreConfigurationParser_3_2 implements ConfigurationParser {
	
	
	@Override
	public void readElement(final ConfigurationReader reader, final ConfigurationBuilderHolder holder) {
		ConfigurationBuilder builder = holder.getCurrentConfigurationBuilder();
		
		Element element = Element.forName(reader.getLocalName());
		switch (element) {
			case SQL_STORE: {
				this.parseSQLStore(reader, builder.persistence(), holder.getClassLoader());
				break;
			}
			default: {
				throw ParseUtils.unexpectedElement(reader);
			}
		}
	}
	
	
	/**
	 * Parses an SQL store XML element.
	 */
	private void parseSQLStore(final ConfigurationReader reader,
				   final PersistenceConfigurationBuilder persistenceBuilder,
				   final ClassLoader classLoader) {
		
		SQLStoreConfigurationBuilder builder = new SQLStoreConfigurationBuilder(persistenceBuilder);
		
		this.parseSQLStoreAttributes(reader, builder, classLoader);
		
		while (reader.hasNext() && (reader.nextElement() != ConfigurationReader.ElementType.END_ELEMENT)) {
			Element element = Element.forName(reader.getLocalName());
			switch (element) {
				default: {
					Parser.parseStoreElement(reader, builder);
					break;
				}
			}
		}
		
		persistenceBuilder.addStore(builder);
	}
	
	
	/**
	 * Parses the attributes of an XML store element.
	 */
	private void parseSQLStoreAttributes(final ConfigurationReader reader,
					     final SQLStoreConfigurationBuilder builder,
					     final ClassLoader classLoader) {
		
		for (int i = 0; i < reader.getAttributeCount(); i++) {
			ParseUtils.requireNoNamespaceAttribute(reader, i);
			String value = replaceProperties(reader.getAttributeValue(i));
			Attribute attribute = Attribute.forName(reader.getAttributeName(i));
			switch (attribute) {
				case RECORD_TRANSFORMER: {
					try {
						builder.recordTransformerClass(classLoader.loadClass(value));
					} catch (ClassNotFoundException e) {
						throw new CacheConfigurationException(e);
					}
					break;
				}
				
				case QUERY_EXECUTOR: {
					try {
						builder.queryExecutorClass(classLoader.loadClass(value));
					} catch (ClassNotFoundException e) {
						throw new CacheConfigurationException(e);
					}
					break;
				}
				
				case SQL_DIALECT: {
					builder.sqlDialect(SQLDialect.valueOf(value.toUpperCase()));
					break;
				}
				
				case CREATE_TABLE_IF_MISSING: {
					if ("true".equals(value)) {
						builder.createTableIfMissing(true);
					} else if ("false".equals(value)){
						builder.createTableIfMissing(false);
					} else {
						throw new CacheConfigurationException("Invalid " + attribute.getLocalName() + ": Must be true or false");
					}
					break;
				}
				
				case CREATE_TABLE_IGNORE_ERRORS: {
					if ("true".equals(value)) {
						builder.createTableIgnoreErrors(true);
					} else if ("false".equals(value)) {
						builder.createTableIgnoreErrors(false);
					} else {
						throw new CacheConfigurationException("Invalid " + attribute.getLocalName() + ": Must be true or false");
					}
					break;
				}
				
				case CONNECTION_POOL: {
					if (value != null && ! value.trim().isEmpty()) {
						builder.connectionPool(value);
					}
					break;
				}

				case EXPIRED_QUERY_PAGE_LIMIT: {
					int pageSize;
					try {
						pageSize = Integer.parseInt(value);
					} catch (NumberFormatException e) {
						throw new CacheConfigurationException("Invalid " + attribute.getLocalName() + ": Must be an integer");
					}
					builder.expiredQueryPageLimit(pageSize);
					break;
				}
				
				default: {
					Parser.parseStoreAttribute(reader, i, builder);
					break;
				}
			}
		}
	}
	
	
	@Override
	public Namespace[] getNamespaces() {
		return ParseUtils.getNamespaceAnnotations(getClass());
	}
}
