package com.nimbusds.infinispan.persistence.sql;


import java.util.Objects;

import org.jooq.Record;


/**
 * Retrieved SQL record implementation.
 */
public class RetrievedSQLRecordImpl implements RetrievedSQLRecord {


	private final Record record;
	
	
	private final boolean fieldsToUpperCase;
	
	
	/**
	 * Creates a new retrieved record.
	 *
	 * @param record The underlying jOOQ record.
	 */
	public RetrievedSQLRecordImpl(final Record record) {
		this(record, false);
	}
	
	
	/**
	 * Creates a new retrieved record.
	 *
	 * @param record            The underlying jOOQ record.
	 * @param fieldsToUpperCase Controls conversion of the field names to
	 *                          upper case.
	 */
	public RetrievedSQLRecordImpl(final Record record,
				      final boolean fieldsToUpperCase) {
		Objects.requireNonNull(record);
		this.record = record;
		this.fieldsToUpperCase = fieldsToUpperCase;
	}
	
	
	@Override
	public Object get(final String fieldName) {
		return record.get(fieldsToUpperCase ? fieldName.toUpperCase() : fieldName);
	}
	
	
	@Override
	public <U> U get(String fieldName, Class<? extends U> type) {
		return record.get(fieldsToUpperCase ? fieldName.toUpperCase() : fieldName, type);
	}
	
	
	@Override
	public int size() {
		return record.size();
	}
	
	
	@Override
	public String toString() {
		return record.toString();
	}
}
