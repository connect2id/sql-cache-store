package com.nimbusds.infinispan.persistence.sql;


import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;

import java.util.Properties;


/**
 * Hikari CP configuration utils.
 */
class HikariConfigUtils {
	
	
	/**
	 * Removes non-Hikari properties to prevent Hikari CP throwing a
	 * configuration exception.
	 *
	 * @param props The properties. Must not be {@code null}.
	 *
	 * @return The conditioned properties.
	 */
	static Properties removeNonHikariProperties(final Properties props) {
		
		var out = new Properties();
		
		for (String key: props.stringPropertyNames()) {
			if (key.startsWith("sqlStore.")) {
				continue; // skip
			}
			
			out.put(key, props.getProperty(key));
		}
		
		return out;
	}


	/**
	 * Removes properties with {@code null}, empty or blank values.
	 *
	 * @param props The properties. Must not be {@code null}.
	 *
	 * @return The conditioned properties.
	 */
	static Properties removeBlankProperties(final Properties props) {

		var out = new Properties();

		for (String key: props.stringPropertyNames()) {
			String value = props.getProperty(key);
			if (value == null || value.trim().isEmpty()) {
				continue; // skip
			}

			out.put(key, value);
		}

		return out;
	}
	
	
	/**
	 * Checks if Hikari CP metrics is already registered for the specified
	 * cache name.
	 *
	 * @param poolName       The Hikari CP name. Must not be {@code null}.
	 * @param metricRegistry The metric registry. Must not be {@code null}.
	 *
	 * @return {@code true} if the Hikari CP metrics are already
	 *         registered.
	 */
	static boolean metricsAlreadyRegistered(final HikariPoolName poolName, final MetricRegistry metricRegistry) {
		
		for (String gaugeName: metricRegistry.getGauges().keySet()) {
			
			// simple test
			if (gaugeName.contains(poolName.toString())) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Checks if Hikari CP health checks are already registered.
	 *
	 * @param poolName            The Hikari CP name. Must not be
	 *                            {@code null}.
	 * @param healthCheckRegistry The health check registry. Must not be
	 *                            {@code null}.
	 *
	 * @return {@code true} if the Hikari CP health checks are already
	 *         registered.
	 */
	static boolean healthChecksAlreadyRegistered(final HikariPoolName poolName, final HealthCheckRegistry healthCheckRegistry) {
		
		for (String checkName: healthCheckRegistry.getNames()) {
			
			// simple test
			if (checkName.contains(poolName.toString()) && checkName.contains("pool.ConnectivityCheck")) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * Prevents public instantiation.
	 */
	private HikariConfigUtils() {}
}
