package com.nimbusds.infinispan.persistence.sql.config;


import org.infinispan.commons.CacheConfigurationException;
import org.infinispan.configuration.cache.AbstractStoreConfiguration;
import org.infinispan.configuration.cache.AbstractStoreConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.jooq.SQLDialect;

import java.util.Properties;


/**
 * SQL store configuration builder.
 *
 * <p>Used by the Infinispan ConfigurationBuilder to implement fluent
 * configuration for the SQL CacheLoader / CacheWriter. Methods should use
 * the fluent style, rather than the setter/getter style and should return an
 * instance of this object.
 */
public class SQLStoreConfigurationBuilder
	extends AbstractStoreConfigurationBuilder<SQLStoreConfiguration,SQLStoreConfigurationBuilder>
	implements SQLStoreConfigurationChildBuilder<SQLStoreConfigurationBuilder> {
	
	
	/**
	 * Creates a new SQL store configuration builder.
	 *
	 * @param builder The general persistence configuration builder.
	 */
	public SQLStoreConfigurationBuilder(final PersistenceConfigurationBuilder builder) {
		super(builder, SQLStoreConfiguration.attributeDefinitionSet());
	}


	@Override
	public SQLStoreConfiguration create() {
        	// This method should construct a new instance of a
		// SQLStoreConfiguration object. There will be one instance
		// per cache.
		return new SQLStoreConfiguration(
			this.attributes.protect(),
			this.async.create());
	}
	
	
	@Override
	public SQLStoreConfigurationBuilder recordTransformerClass(final Class recordTransformerClass) {
		
		this.attributes.attribute(SQLStoreConfiguration.RECORD_TRANSFORMER).set(recordTransformerClass);
		return this;
	}
	
	
	@Override
	public SQLStoreConfigurationBuilder queryExecutorClass(final Class queryExecutorClass) {
		
		this.attributes.attribute(SQLStoreConfiguration.QUERY_EXECUTOR).set(queryExecutorClass);
		return this;
	}
	
	
	@Override
	public SQLStoreConfigurationBuilder sqlDialect(final SQLDialect sqlDialect) {
		
		this.attributes.attribute(SQLStoreConfiguration.SQL_DIALECT).set(sqlDialect);
		return this;
	}
	
	
	@Override
	public SQLStoreConfigurationBuilder createTableIfMissing(final boolean createTableIfMissing) {
		
		this.attributes.attribute(SQLStoreConfiguration.CREATE_TABLE_IF_MISSING).set(createTableIfMissing);
		return this;
	}
	
	
	@Override
	public SQLStoreConfigurationBuilder createTableIgnoreErrors(final boolean createTableIgnoreErrors) {
		
		this.attributes.attribute(SQLStoreConfiguration.CREATE_TABLE_IGNORE_ERRORS).set(createTableIgnoreErrors);
		return this;
	}
	
	
	@Override
	public SQLStoreConfigurationBuilder connectionPool(final String cacheName) {
		
		this.attributes.attribute(SQLStoreConfiguration.CONNECTION_POOL).set(cacheName);
		return this;
	}


	@Override
	public SQLStoreConfigurationBuilder expiredQueryPageLimit(final int pageLimit) {

		if (pageLimit < 1) {
			throw new IllegalArgumentException("The expired query page limit must a positive integer");
		}

		this.attributes.attribute(SQLStoreConfiguration.EXPIRED_QUERY_PAGE_LIMIT).set(pageLimit);
		return this;
	}


	@Override
	public SQLStoreConfigurationBuilder withProperties(final Properties properties) {
		
		return properties(properties);
	}
	
	
	@Override
	public void validate() {
		
		super.validate();
		
		if (this.attributes.attribute(SQLStoreConfiguration.RECORD_TRANSFORMER).get() == null) {
			throw new CacheConfigurationException("An SQL store record transformer class must be specified");
		}
		
		if (this.attributes.attribute(SQLStoreConfiguration.SQL_DIALECT).get() == null) {
			throw new CacheConfigurationException("An SQL store dialect must be specified");
		}
		
		Properties props = this.attributes.attribute(AbstractStoreConfiguration.PROPERTIES).get();
		
		if (this.attributes.attribute(SQLStoreConfiguration.CONNECTION_POOL).get() == null && (props == null || props.isEmpty())) {
			throw new CacheConfigurationException("Missing SQL store properties, such as jdbcUrl, check the service documentation");
		}

		if (this.attributes.attribute(SQLStoreConfiguration.EXPIRED_QUERY_PAGE_LIMIT).get() < 1) {
			throw new CacheConfigurationException("The expired select query page size must be greater than zero");
		}
	}


	@Override
	public SQLStoreConfigurationBuilder self() {
		return this;
	}
}
