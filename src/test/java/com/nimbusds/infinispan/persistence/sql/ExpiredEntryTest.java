package com.nimbusds.infinispan.persistence.sql;


import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;
import net.jcip.annotations.NotThreadSafe;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


@NotThreadSafe
@RunWith(Parameterized.class)
public class ExpiredEntryTest extends TestWithSQLDatabase {
	
	
	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}
	
	public static final String CACHE_NAME = "userMap";
	
	private EmbeddedCacheManager cacheMgr;
	
	private Cache<String,User> myMap;
	
	private DSLContext dsl;
	
	
	public ExpiredEntryTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
	}
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {

		super.setUp();

		Properties props = getTestProperties(sqlDialect);
		
		cacheMgr = new DefaultCacheManager();
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(SQLStoreConfigurationBuilder.class)
			.segmented(false)
			.recordTransformerClass(ExpiredEntryReaperTest.ExpiringUserRecordTransformer.class)
			.queryExecutorClass(UserQueryExecutor.class)
			.sqlDialect(sqlDialect)
			.createTableIfMissing(true) // optional, defaults to true
			.withProperties(props)
			.create();
		
		b.memory()
			.maxCount(100)
			.whenFull(EvictionStrategy.REMOVE)
			.create();
		
		b.expiration()
			.wakeUpInterval(1L, TimeUnit.MINUTES)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
		
		myMap = cacheMgr.getCache(CACHE_NAME);
		
		dsl = DSL.using(((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getDataSource().getConnection(), sqlDialect, null);
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down DB
		super.tearDown();
	}
	

	// expired entries keys must start with "exp-"
	// active entries keys must start with "act-"
	private void storeEntry(final String key) {
		
		var user = new User(
			"Alice Adams",
			"alice@wonderland.net",
			Instant.now().minus(1L, ChronoUnit.DAYS),
			Collections.singletonList("admin"));
		
		UserRecordTransformer t = new ExpiredEntryReaperTest.ExpiringUserRecordTransformer();
		t.init(() -> sqlDialect);
		
		SQLRecord sqlRecord = t.toSQLRecord(new InfinispanEntry<>(
			key,
			user,
			new InternalMetadataBuilder()
				.created(Date.from(Instant.now().minus(1L, ChronoUnit.DAYS)))
				.lifespan(1L, TimeUnit.HOURS)
				.build())
		);
		
		dsl.mergeInto(table(t.getTableName()), sqlRecord.getFields().keySet())
			.key(sqlRecord.getKeyColumns())
			.values(sqlRecord.getFields().values())
			.execute();
	}
	
	
	@Test
	public void testDoNotReturnExpiredEntry() {
		
		String key = "exp-u1";
		storeEntry(key);
		
		assertEquals(1, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
		
		assertNull(myMap.get(key));
	}
	
	
	@Test
	public void testDoNotProcessExpiredEntry() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String key = "exp-u1";
		storeEntry(key);
		
		assertEquals(1, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
		
		Map<String,User> filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertTrue(filteredMap.isEmpty());
	}
	
	
	@Test
	public void testPurge()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String key = "exp-u1";
		storeEntry(key);
		
		assertEquals(1, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
		
		myMap.getAdvancedCache().getExpirationManager().processExpiration();
		
		Thread.sleep(100);
		
		assertEquals(0, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
	}
	
	
	@Test
	public void testPurgeMultiple()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		final int expiredCount = 250;
		IntStream.range(0, expiredCount).forEach(i-> storeEntry("exp-u" + i));
		assertEquals(expiredCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
		myMap.getAdvancedCache().getExpirationManager().processExpiration();
		
		Thread.sleep(100);
		
		assertEquals(0, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
	}


	@Test
	public void testPurgeMultiple_withNonExpiredPresent()
		throws Exception {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		final int expiredCount = 250;
		IntStream.range(0, expiredCount).forEach(i-> storeEntry("exp-u" + i));
		assertEquals(expiredCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());

		final int activeCount = 50;
		IntStream.range(0, activeCount).forEach(i-> storeEntry("act-u" + i));
		assertEquals(expiredCount + activeCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());

		myMap.getAdvancedCache().getExpirationManager().processExpiration();

		Thread.sleep(100);

		assertEquals(activeCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
	}


	@Test
	public void testPurgeMultiple_withNonExpiredPresent_withBadPresent()
		throws Exception {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		final int badCount = 10;
		IntStream.range(0, badCount).forEach(i-> storeEntry("bad-u" + i));
		assertEquals(badCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());

		final int expiredCount = 250;
		IntStream.range(0, expiredCount).forEach(i-> storeEntry("exp-u" + i));
		assertEquals(badCount + expiredCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());

		final int activeCount = 50;
		IntStream.range(0, activeCount).forEach(i-> storeEntry("act-u" + i));
		assertEquals(badCount + expiredCount + activeCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());

		myMap.getAdvancedCache().getExpirationManager().processExpiration();

		Thread.sleep(100);

		assertEquals(badCount + activeCount, dsl.select().from(table(new UserRecordTransformer().getTableName())).fetch().size());
	}
}
