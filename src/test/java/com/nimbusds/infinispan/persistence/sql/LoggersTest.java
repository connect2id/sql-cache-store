package com.nimbusds.infinispan.persistence.sql;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class LoggersTest {
	

	@Test
	public void testNames() {

		assertEquals("MAIN", Loggers.MAIN_LOG.getName());
		assertEquals("SQL", Loggers.SQL_LOG.getName());
	}
}
