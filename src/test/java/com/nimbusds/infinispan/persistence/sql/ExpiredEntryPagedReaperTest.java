package com.nimbusds.infinispan.persistence.sql;


import com.codahale.metrics.Timer;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import net.jcip.annotations.NotThreadSafe;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.jooq.OrderField;
import org.jooq.SQLDialect;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


@NotThreadSafe
@RunWith(Parameterized.class)
public class ExpiredEntryPagedReaperTest extends TestWithSQLDatabase {


	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}


	public static class PagedExpiringUserRecordTransformer extends ExpiredEntryReaperTest.ExpiringUserRecordTransformer {

		@Override
		public Collection<OrderField<?>> getKeyColumnsForExpiredEntryReaper() {
			return Collections.singletonList(field("id"));
		}

		@Override
		public List<Object> getKeyValuesForExpiredEntryReaper(final String key) {
			return Collections.singletonList(key);
		}
	}


	private SQLRecordTransformer<String,User> entityTransformer;


	public ExpiredEntryPagedReaperTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
	}
	
	
	@Before
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		entityTransformer = new PagedExpiringUserRecordTransformer();
		entityTransformer.init(() -> sqlDialect);
		
		// Create table
		sql.execute(entityTransformer.getCreateTableStatement());

		// Insert expired
		for (int i=0; i < 1000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>("exp-" + i, user, null)).getFields())
				.execute();
			
			assertEquals(1, inserted);
		}

		// Insert active
		for (int i=0; i < 500; ++i) {

			User user = new User("Alice " + i, "hello-" + i + "@email.com");

			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>("act-" + i, user, null)).getFields())
				.execute();

			assertEquals(1, inserted);
		}
		
		assertEquals(1500, sql.fetchCount(table(entityTransformer.getTableName())));
	}


	@Test
	public void testConstants() {

		assertEquals(100, ExpiredEntryReaper.DELETE_BATCH_SIZE);
	}


	@Test
	public void testExpireWithKeyListener() {

		var deleteTimer = new Timer();

		var reaper = new ExpiredEntryPagedReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, 100, deleteTimer);

		List<String> deletedKeys = new LinkedList<>();

		// Purge expired entries (all must be marked as such)
		reaper.purgeWithKeyListener(
			key -> {
				assertTrue(key.startsWith("exp-"));
				deletedKeys.add(key);
			});

		// Check deleted keys pushed to listener
		for (int i=0; i < 1000; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}

		assertEquals(1000, deletedKeys.size());

		assertEquals(1000, deleteTimer.getCount());

		assertEquals("Remaining non-expired", 500, sql.fetchCount(table(entityTransformer.getTableName())));
	}
	
	
	@Test
	public void testExpireWithEntryListener() {

		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryPagedReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, 100, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purgeWithEntryListener(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<>() {
				
				@Override
				public void marshalledEntryPurged(MarshallableEntry<String, User> entry) {
					assertTrue(entry.getKey().startsWith("exp-"));
					deletedKeys.add(entry.getKey());
					assertNotNull(entry.getValue());
					assertNotNull(entry.getMetadata());
				}
				
				
				@Override
				public void entryPurged(String s) {
					fail(); // never called
				}
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 1000; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}
		
		assertEquals(1000, deletedKeys.size());

		assertEquals(1000, deleteTimer.getCount());

		assertEquals("Remaining non-expired", 500, sql.fetchCount(table(entityTransformer.getTableName())));
	}
}
