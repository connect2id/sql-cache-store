package com.nimbusds.infinispan.persistence.sql;


import java.util.function.Consumer;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.query.Query;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import com.nimbusds.infinispan.persistence.common.query.UnsupportedQueryException;
import com.nimbusds.infinispan.persistence.sql.query.SQLQueryExecutor;
import com.nimbusds.infinispan.persistence.sql.query.SQLQueryExecutorInitContext;


/**
 * User SQL query executor implementation.
 */
public class UserQueryExecutor implements SQLQueryExecutor<String,User> {
	
	
	SQLQueryExecutorInitContext<String,User> initCtx;
	
	
	@Override
	public void init(final SQLQueryExecutorInitContext<String,User> initCtx) {
		this.initCtx = initCtx;
	}
	
	
	private RetrievedSQLRecord wrap(final Record record) {
		return new RetrievedSQLRecordImpl(record, SQLDialect.ORACLE.equals(initCtx.getSQLDialect()));
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public void executeQuery(final Query query, final Consumer<InfinispanEntry<String,User>> consumer) {
		
		if (! (query instanceof SimpleMatchQuery)) {
			throw new UnsupportedQueryException(query);
		}
		
		var matchQuery = (SimpleMatchQuery<String,User>)query;
		
		Result<Record> result = DSL.using(initCtx.getDataSource(), initCtx.getSQLDialect())
			.selectFrom(table(initCtx.getSQLRecordTransformer().getTableName()))
			.where(field(matchQuery.getKey()).equal(matchQuery.getValue()))
			.fetch();
		
		result.forEach(record -> consumer.accept(initCtx.getSQLRecordTransformer().toInfinispanEntry(wrap(record))));
	}
}
