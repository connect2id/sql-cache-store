package com.nimbusds.infinispan.persistence.sql;


import org.jooq.OrderField;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.jooq.impl.DSL.field;


/**
 * Transforms User POJO to / from SQL record.
 */
public class UserRecordTransformerWithPagedPurgeSupport extends UserRecordTransformer {


	@Override
	public Collection<OrderField<?>> getKeyColumnsForExpiredEntryReaper() {
		return Collections.singletonList(field("id"));
	}

	@Override
	public List<Object> getKeyValuesForExpiredEntryReaper(final String key) {
		return Collections.singletonList(key);
	}
}
