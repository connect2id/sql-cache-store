package com.nimbusds.infinispan.persistence.sql;


import java.util.Collections;
import java.util.List;

import org.jooq.SQLDialect;


public class UserRecordAndTableTransformer extends UserRecordTransformer implements SQLTableTransformer {
	
	
	@Override
	public List<String> getTransformTableStatements(List<String> columnNames) {
		
		if (columnNames.contains("address")) {
			return null; // skip
		}
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE test_users " +
				"ADD COLUMN IF NOT EXISTS address VARCHAR(255) " +
				"AFTER email");
		}
		
		if (SQLDialect.MYSQL.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE test_users " +
				"ADD COLUMN address VARCHAR(255) " +
				"AFTER email");
		}
		
		if (SQLDialect.POSTGRES_9_5.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE test_users " +
				"ADD COLUMN address VARCHAR(255)");
		}
		
		if (SQLDialect.SQLSERVER2016.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE test_users " +
				"ADD address VARCHAR(255)");
		}
		
		if (SQLDialect.ORACLE.equals(sqlDialect)) {
			
			return Collections.singletonList("ALTER TABLE test_users " +
				"ADD address VARCHAR(255)");
		}
		
		return null;
	}
}
