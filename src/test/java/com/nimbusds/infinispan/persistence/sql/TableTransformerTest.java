package com.nimbusds.infinispan.persistence.sql;


import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;

import net.jcip.annotations.NotThreadSafe;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.jooq.SQLDialect;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;


/**
 * Tests the SQL store with a programmatic config and a table transformer.
 */
@NotThreadSafe
@RunWith(Parameterized.class)
public class TableTransformerTest extends TestWithSQLDatabase {
	
	
	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}


	public static final String CACHE_NAME = "myMap";


	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;
	
	
	public TableTransformerTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
	}
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {

		super.setUp();
		
		Properties props = getTestProperties(sqlDialect);

		cacheMgr = new DefaultCacheManager();

		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(SQLStoreConfigurationBuilder.class)
			.segmented(false)
			.recordTransformerClass(UserRecordAndTableTransformer.class) // implements SQLTableTransformer
			.queryExecutorClass(UserQueryExecutor.class)
			.sqlDialect(sqlDialect)
			.createTableIfMissing(true) // optional, defaults to true
			.withProperties(props)
			.create();

		b.memory()
			.maxCount(100)
			.whenFull(EvictionStrategy.REMOVE)
			.create();

		b.expiration()
			.wakeUpInterval(50L, TimeUnit.MILLISECONDS)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down DB
		super.tearDown();
	}
	
	
	@Test
	public void testGetSQLRecordTransformer() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		SQLRecordTransformer<String, User> tr = ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getSQLRecordTransformer();
		
		assertTrue(tr instanceof UserRecordAndTableTransformer);
		
		assertEquals(new UserRecordAndTableTransformer().getTableName(), tr.getTableName());
	}
	
	
	@Test
	public void testTableColumnAdded() throws InterruptedException {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		Thread.sleep(500); // table column alter async
		
		Set<String> columnNames = new HashSet<>(SQLTableUtils.getColumnNames(table("test_users"), sql));
		
		Set<String> expectedCols = Set.of("id", "given_name", "surname", "email", "address", "created", "permissions");

		assertEquals(expectedCols, columnNames);
	}
}