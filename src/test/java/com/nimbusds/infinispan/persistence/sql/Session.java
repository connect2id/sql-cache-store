package com.nimbusds.infinispan.persistence.sql;


import net.jcip.annotations.Immutable;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;


/**
 * Session POJO, test class.
 */
@Immutable
public final class Session {


        private final String name;


        private final String data;


        public Session(final String userName, final String data) {
                assert userName != null;
                this.name = userName;
                this.data = data;
        }


        public String getName() {
                return name;
        }


        public String getData() {
                return data;
        }


        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (!(o instanceof Session session)) return false;
                return Objects.equals(name, session.name) && Objects.equals(data, session.data);
        }


        @Override
        public int hashCode() {
                return Objects.hash(name, data);
        }


        @Override
        public String toString() {
                return new ToStringBuilder(this)
                        .append("userName", name)
                        .append("data", data)
                        .toString();
        }
}
