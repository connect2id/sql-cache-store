package com.nimbusds.infinispan.persistence.sql;


import java.util.Properties;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

import org.infinispan.commons.util.StringPropertyReplacer;
import org.junit.Test;


public class StringPropertyReplacerTest {
	
	@Test
	public void testNoReplace() {
		
		var props = new Properties();
		props.setProperty("sqlStore.sqlDialect", "H2");
		String out = StringPropertyReplacer.replaceProperties("MySQL", props);
		assertEquals("MySQL", out);
	}
	
	@Test
	public void testReplace() {
		
		var props = new Properties();
		props.setProperty("sqlStore.sqlDialect", "H2");
		String out = StringPropertyReplacer.replaceProperties("${sqlStore.sqlDialect}", props);
		assertEquals("H2", out);
	}
	
	@Test
	public void testReplaceUsingSystemProperties() {
		
		String randomName = UUID.randomUUID().toString();
		
		System.getProperties().setProperty(randomName, "abc");
		
		String out = StringPropertyReplacer.replaceProperties("${" + randomName + "}");
		assertEquals("abc", out);
		
		System.getProperties().remove(randomName);
	}
	
	@Test
	public void testReplaceDefault() {
		
		var props = new Properties();
		String out = StringPropertyReplacer.replaceProperties("${sqlStore.sqlDialect:H2}", props);
		assertEquals("H2", out);
	}
}
