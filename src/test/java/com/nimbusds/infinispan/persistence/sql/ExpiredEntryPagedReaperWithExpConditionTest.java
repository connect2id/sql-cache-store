package com.nimbusds.infinispan.persistence.sql;


import com.codahale.metrics.Timer;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import net.jcip.annotations.NotThreadSafe;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.jooq.SQLDialect;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


@NotThreadSafe
@RunWith(Parameterized.class)
public class ExpiredEntryPagedReaperWithExpConditionTest extends TestWithSQLDatabase {


	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}


	private SQLRecordTransformer<String,Session> entityTransformer;


	public ExpiredEntryPagedReaperWithExpConditionTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
	}
	
	
	@Before
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		entityTransformer = new SessionTransformer();
		entityTransformer.init(() -> sqlDialect);
		
		// Create table
		sql.execute(entityTransformer.getCreateTableStatement());

		// Insert expired
		InternalMetadata expMetadata = new InternalMetadataBuilder()
			.created(Instant.now().minus(1, ChronoUnit.DAYS).toEpochMilli())
			.lifespan(1, TimeUnit.HOURS)
			.build();

		for (int i=0; i < 1000; ++i) {

			var key = "exp-" + i;
			var value = new Session("val-" + i, "{}");

			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>(key, value, expMetadata)).getFields())
				.execute();
			
			assertEquals(1, inserted);
		}

		// Insert active
		InternalMetadata actMetadata = new InternalMetadataBuilder()
			.created(Instant.now().toEpochMilli())
			.lifespan(1, TimeUnit.HOURS)
			.build();

		for (int i=0; i < 500; ++i) {

			var key = "act-" + i;
			var value = new Session("val-" + i, "{}");

			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>(key, value, actMetadata)).getFields())
				.execute();

			assertEquals(1, inserted);
		}
		
		assertEquals(1500, sql.fetchCount(table(entityTransformer.getTableName())));
	}


	@Test
	public void testExpireWithKeyListener() {

		var deleteTimer = new Timer();

		var reaper = new ExpiredEntryPagedReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, 100, deleteTimer);

		List<String> deletedKeys = new LinkedList<>();

		// Purge expired entries (all must be marked as such)
		reaper.purgeWithKeyListener(
			key -> {
				assertTrue(key.startsWith("exp-"));
				deletedKeys.add(key);
			});

		// Check deleted keys pushed to listener
		for (int i=0; i < 1000; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}

		assertEquals(1000, deletedKeys.size());

		assertEquals(1000, deleteTimer.getCount());

		assertEquals("Remaining non-expired", 500, sql.fetchCount(table(entityTransformer.getTableName())));
	}
	
	
	@Test
	public void testExpireWithEntryListener() {

		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryPagedReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, 100, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purgeWithEntryListener(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<>() {
				
				@Override
				public void marshalledEntryPurged(MarshallableEntry<String, Session> entry) {
					assertTrue(entry.getKey().startsWith("exp-"));
					deletedKeys.add(entry.getKey());
					assertNotNull(entry.getValue());
					assertNotNull(entry.getMetadata());
				}
				
				
				@Override
				public void entryPurged(String s) {
					fail(); // never called
				}
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 1000; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}
		
		assertEquals(1000, deletedKeys.size());

		assertEquals(1000, deleteTimer.getCount());

		assertEquals("Remaining non-expired", 500, sql.fetchCount(table(entityTransformer.getTableName())));
	}
}
