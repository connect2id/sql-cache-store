package com.nimbusds.infinispan.persistence.sql;


import java.util.*;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang3.RandomStringUtils;
import org.jooq.SQLDialect;
import org.junit.Test;


public class JOOQFixesTest {
	

	@Test
	public void testExtractPrimaryKeyConstraintName() {
		
		String stmt =
			"CREATE TABLE IF NOT EXISTS test_users (" +
			"id VARCHAR(255) NOT NULL," +
			"given_name VARCHAR(255) NULL," +
			"surname VARCHAR(255) NULL," +
			"email VARCHAR(255) NULL," +
			"created TIMESTAMP NULL," +
			"permissions TEXT[] NULL," +
			"CONSTRAINT pk PRIMARY KEY (id)" +
			")";
		
		assertEquals("pk", JOOQFixes.parsePrimaryKeyConstraintName(stmt));
	}
	

	@Test
	public void testExtractPrimaryKeyConstraintName_lowerCase() {
		
		String stmt =
			"CREATE TABLE IF NOT EXISTS test_users (" +
			"id VARCHAR(255) NOT NULL," +
			"given_name VARCHAR(255) NULL," +
			"surname VARCHAR(255) NULL," +
			"email VARCHAR(255) NULL," +
			"created TIMESTAMP NULL," +
			"permissions TEXT[] NULL," +
			"CONSTRAINT pk PRIMARY KEY (id)" +
			")";
		
		assertEquals("pk", JOOQFixes.parsePrimaryKeyConstraintName(stmt.toLowerCase()));
	}
	

	@Test
	public void testExtractPrimaryKeyConstraintName_extraWhiteSpace() {
		
		String stmt =
			"CREATE TABLE IF NOT EXISTS test_users (" +
			"id VARCHAR(255) NOT NULL," +
			"given_name VARCHAR(255) NULL," +
			"surname VARCHAR(255) NULL," +
			"email VARCHAR(255) NULL," +
			"created TIMESTAMP NULL," +
			"permissions TEXT[] NULL," +
			"CONSTRAINT pk PRIMARY KEY (id)" +
			")";
		
		assertEquals("pk", JOOQFixes.parsePrimaryKeyConstraintName(stmt.toLowerCase()));
	}
	
	
	@Test
	public void testOracleCLOBConstant() {
		
		assertEquals(4000 / 2, JOOQFixes.ORACLE_STRING_LITERAL_CUTOFF_LENGTH);
	}
	
	
	@Test
	public void testSplitChunksForOracleWriteClob() {
		
		String in = "";
		List<String> out = JOOQFixes.splitChunksForOracleWriteClob(in);
		assertEquals(Collections.singletonList(in), out);
		
		in = "abc";
		out = JOOQFixes.splitChunksForOracleWriteClob(in);
		assertEquals(Collections.singletonList(in), out);
		
		in = RandomStringUtils.random(JOOQFixes.ORACLE_STRING_LITERAL_CUTOFF_LENGTH);
		out = JOOQFixes.splitChunksForOracleWriteClob(in);
		assertEquals(Collections.singletonList(in), out);
		
		String in_1 = RandomStringUtils.random(JOOQFixes.ORACLE_STRING_LITERAL_CUTOFF_LENGTH);
		String in_2 = RandomStringUtils.random(JOOQFixes.ORACLE_STRING_LITERAL_CUTOFF_LENGTH);
		String in_3 = RandomStringUtils.random(JOOQFixes.ORACLE_STRING_LITERAL_CUTOFF_LENGTH);
		out = JOOQFixes.splitChunksForOracleWriteClob(in_1 + in_2 + in_3);
		assertEquals(List.of(in_1, in_2, in_3), out);
	}
	
	
	@Test
	public void testConcatChunksForOracleWriteClob() {
		
		String in_1 = UUID.randomUUID().toString();
		String in_2 = UUID.randomUUID().toString();
		
		String out = JOOQFixes.concatChunksForOracleWriteClob(List.of(in_1));
		assertEquals("TO_CLOB('" + in_1 + "')", out);
		
		out = JOOQFixes.concatChunksForOracleWriteClob(List.of(in_1, in_2));
		assertEquals("TO_CLOB('" + in_1 + "')||TO_CLOB('" + in_2 + "')", out);
	}


	@Test
	public void testConcatChunksForOracleWriteClob_escapeQuoteChar() {

		String in_1 = "I'll be back";
		String in_2 = "Cicero's De Finibus Bonorum et Malorum";

		String out = JOOQFixes.concatChunksForOracleWriteClob(List.of(in_1));
		assertEquals("TO_CLOB('I''ll be back')", out);

		out = JOOQFixes.concatChunksForOracleWriteClob(List.of(in_1, in_2));
		assertEquals("TO_CLOB('I''ll be back')||TO_CLOB('Cicero''s De Finibus Bonorum et Malorum')", out);
	}
	
	
	@Test
	public void testCompleteOracleWriteClob() {
		
		String code = "833b2c39-6b64-4bdc-8a55-3c29bc3f1156";
		
		var sqlStatement = "merge into test_users" +
			"using (" +
			"  select" +
			"    'a35' \"s1\"," +
			"    'Alice' \"s2\"," +
			"    'Adams-35' \"s3\"," +
			"    '" + code + "' \"s4\"," + // here
			"    null \"s5\"," +
			"    null \"s6\"" +
			"  from DUAL" +
			") \"src\"" +
			"on (id = \"src\".\"s1\")" +
			"when matched then update set" +
			"  given_name = \"src\".\"s2\"," +
			"  surname = \"src\".\"s3\"," +
			"  email = \"src\".\"s4\"," +
			"  created = \"src\".\"s5\"," +
			"  permissions = \"src\".\"s6\"" +
			"when not matched then insert (" +
			"  id," +
			"  given_name," +
			"  surname," +
			"  email," +
			"  created," +
			"  permissions" +
			")" +
			"values (" +
			"  \"src\".\"s1\", " +
			"  \"src\".\"s2\", " +
			"  \"src\".\"s3\", " +
			"  \"src\".\"s4\", " +
			"  \"src\".\"s5\", " +
			"  \"src\".\"s6\"" +
			")";
		
		
		for (SQLDialect sqlDialect: SQLStore.SUPPORTED_DATABASES) {
			
			var t = new UserRecordTransformer();
			t.init(() -> sqlDialect);
			
			var f = new JOOQFixes(sqlDialect);
			
			String out = f.completeOracleWriteClob(
				sqlStatement,
				Map.of(
					code,
					Collections.singletonList("alice-35@email.com")
				));
			
			if (SQLDialect.ORACLE.equals(sqlDialect)) {
				assertEquals(sqlStatement.replace("'"+code+"'", "TO_CLOB('alice-35@email.com')"), out);
			} else {
				assertEquals(sqlStatement, out);
			}
		}
		
	}
}
