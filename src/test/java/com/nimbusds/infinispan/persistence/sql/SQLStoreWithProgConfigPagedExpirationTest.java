package com.nimbusds.infinispan.persistence.sql;


import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;
import com.nimbusds.infinispan.persistence.sql.transformers.SQLFieldTransformer;
import net.jcip.annotations.NotThreadSafe;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


/**
 * Tests the SQL store with a programmatic config.
 */
@NotThreadSafe
@RunWith(Parameterized.class)
public class SQLStoreWithProgConfigPagedExpirationTest extends TestWithSQLDatabase {


	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}


	private static final String CACHE_NAME = "userMap";


	private EmbeddedCacheManager cacheMgr;


	private final SQLFieldTransformer fieldTransformer;


	private static Connection getSQLConnection()
		throws SQLException {

		return ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getDataSource().getConnection();
	}


	public SQLStoreWithProgConfigPagedExpirationTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
		fieldTransformer = new SQLFieldTransformer(sqlDialect);
	}
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {

		super.setUp();
		
		Properties props = getTestProperties(sqlDialect);

		cacheMgr = new DefaultCacheManager();

		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(SQLStoreConfigurationBuilder.class)
			.segmented(false)
			.recordTransformerClass(UserRecordTransformerWithPagedPurgeSupport.class)
			.queryExecutorClass(UserQueryExecutor.class)
			.sqlDialect(sqlDialect)
			.createTableIfMissing(true) // optional, defaults to true
			.withProperties(props)
			.create();

		b.memory()
			.maxCount(100)
			.whenFull(EvictionStrategy.REMOVE)
			.create();

		b.expiration()
			.wakeUpInterval(1L, TimeUnit.HOURS)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down DB
		super.tearDown();
	}
	
	
	@Test
	public void testGetSQLRecordTransformer() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		SQLRecordTransformer<String, User> tr = ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getSQLRecordTransformer();
		
		assertEquals(new UserRecordTransformerWithPagedPurgeSupport().getTableName(), tr.getTableName());
	}
	
	
	@Test
	public void testSimpleObjectLifeCycle()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Get non-existing object
		assertNull(myMap.get("invalid-key"));
		
		// Store new object with all fields defined
		Instant created = Instant.ofEpochMilli(1474012953000L);
		var u1 = new User("Alice Adams", "alice@wonderland.net", created, Arrays.asList("admin", "audit"));
		assertNull(myMap.putIfAbsent("u1", u1));
		
		// Check presence
		assertTrue(myMap.containsKey("u1"));
		
		
		// Confirm SQL insert
		RetrievedSQLRecord r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Alice",  r.get("given_name"));
		assertEquals("Adams",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(new String[]{"admin", "audit"}, fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
	
		
		// Get new count
		assertEquals(1, myMap.size());
		
		// Get object
		User out = myMap.get("u1");
		assertEquals(u1.getName(), out.getName());
		assertEquals(u1.getEmail(), out.getEmail());
		assertEquals(u1.getCreated(), out.getCreated());
		assertEquals(u1.getPermissions(), out.getPermissions());
		
		// Update object
		var u2 = new User("Bob Brown", "bob@wonderland.net", created, Arrays.asList("browse", "pay"));
		assertNotNull(myMap.replace("u1", u2));
		
		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Bob",  r.get("given_name"));
		assertEquals("Brown",  r.get("surname"));
		assertEquals("bob@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(new String[]{"browse", "pay"}, fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u2.getName(), out.getName());
		assertEquals(u2.getEmail(), out.getEmail());
		assertEquals(u2.getCreated(), out.getCreated());
		assertEquals(u2.getPermissions(), out.getPermissions());
		
		// Update object with fewer defined fields
		var u3 = new User("Claire Cox", "claire@wonderland.net");
		assertNotNull(myMap.replace("u1", u3));
		
		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Claire",  r.get("given_name"));
		assertEquals("Cox",  r.get("surname"));
		assertEquals("claire@wonderland.net",  r.get("email"));
		assertNull(r.get("created")); // Timestamp
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		
		// Remove object
		out = myMap.remove("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		// Confirm removal
		assertNull(myMap.get("u1"));
		assertFalse(myMap.containsKey("u1"));
		
		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		}
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}
	
	
	@Test
	public void testPutExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "alice";
		var u1 = new User("Alice Adams", "alice@name.com");
		
		myMap.put(k1, u1, 60L, TimeUnit.HOURS);
		
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		myMap.put(k1, u1, 60L, TimeUnit.HOURS);
	}
	
	
	@Test
	public void testPurgeNonExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			var value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Nothing to purge with regular user records
		
		SQLStore.getInstances().get(CACHE_NAME).purge(Executors.newSingleThreadExecutor(), new AdvancedCacheExpirationWriter.ExpirationPurgeListener() {
			
			@Override
			public void marshalledEntryPurged(MarshallableEntry entry) {
				fail();
			}
			
			
			@Override
			public void entryPurged(Object o) {
				fail();
			}
		});
		
		assertEquals(200, myMap.size());
	}
}