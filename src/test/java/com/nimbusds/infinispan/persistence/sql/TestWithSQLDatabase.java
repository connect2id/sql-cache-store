package com.nimbusds.infinispan.persistence.sql;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.After;
import org.junit.Before;


/**
 * The base class for tests that require an SQL database.
 */
public abstract class TestWithSQLDatabase {
	
	
	public static Properties getTestProperties(final SQLDialect sqlDialect) {
		
		var props = new Properties();
		try {
			props.load(new FileInputStream("src/test/resources/jdbc-" + sqlDialect.getName().toLowerCase() + ".properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return props;
	}
	
	
	/**
	 * The SQL dialect.
	 */
	protected final SQLDialect sqlDialect;
	
	
	/**
	 * Keep this SQL connection for the duration of the test. Required by
	 * the H2 test to prevent the in memory DB from stopping.
	 */
	protected Connection persistentConnection;
	
	
	/**
	 * DSL context wrapping the {@link #persistentConnection}.
	 */
	protected DSLContext sql;
	
	
	public TestWithSQLDatabase(final SQLDialect sqlDialect) {
		this.sqlDialect = sqlDialect;
	}
	
	
	@Before
	public void setUp()
		throws Exception {
		
		Properties props = getTestProperties(sqlDialect);
		
		String url = props.getProperty("jdbcUrl");
		String userName = props.getProperty("username");
		String password = props.getProperty("password");
		
		// Connection is the only JDBC resource that we need
		// PreparedStatement and ResultSet are handled by jOOQ, internally
		persistentConnection = DriverManager.getConnection(url, userName, password);
		
		System.out.println("Setting up " + sqlDialect + " test database " + url);
		
		sql = DSL.using(persistentConnection, sqlDialect);
	}
	
	
	public RetrievedSQLRecord wrap(final Record record) {
		
		return new RetrievedSQLRecordImpl(record, SQLDialect.ORACLE.equals(sqlDialect));
	}

	
	@After
	public void tearDown()
		throws Exception {
		
		dropTables();
		
		if (persistentConnection != null) {
			persistentConnection.close();
		}
	}
	
	
	protected void dropTables() {
		
		dropTable(UserRecordTransformer.TABLE_NAME);
		dropTable(SessionTransformer.TABLE_NAME);
	}


	protected void dropTable(final String tableName) {

		if (sql == null) {
			return;
		}

		try {
			if (SQLDialect.ORACLE.equals(sqlDialect)) {
				sql.execute("drop table " + tableName + " cascade constraints purge");
				System.out.println("Dropped " + tableName + " table");
			} else {
				if (sql.dropTableIfExists(tableName).execute() > 0) {
					System.out.println("Dropped " + tableName + " table");
				}
			}

		} catch (Exception e) {
			System.out.println("Drop table exception: " + e.getMessage());
		}
	}


	public static String toSQLTimestamp(final Instant instant) {
		
		// Timestamp in local timezone
		OffsetDateTime dt = OffsetDateTime.ofInstant(instant, ZoneId.systemDefault());
		return DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss.S").format(dt);
	}
}
