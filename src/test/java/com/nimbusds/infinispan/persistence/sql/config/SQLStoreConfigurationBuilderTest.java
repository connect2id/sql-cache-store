package com.nimbusds.infinispan.persistence.sql.config;


import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.junit.Test;

import static org.junit.Assert.*;


public class SQLStoreConfigurationBuilderTest {
	

	@Test
	public void testConstructor_simple() {
		
		PersistenceConfigurationBuilder persistenceConfigurationBuilder = new ConfigurationBuilder().persistence();
		
		var b = new SQLStoreConfigurationBuilder(persistenceConfigurationBuilder);
		
		SQLStoreConfiguration config = b.create();
		
		// Infinispan
		assertTrue(config.segmented());
		assertFalse(config.purgeOnStartup());
		assertFalse(config.fetchPersistentState());
		assertFalse(config.ignoreModifications());
		assertNotNull(config.async());
		assertFalse(config.async().enabled());
		assertFalse(config.preload());
		assertFalse(config.shared());
		assertTrue(config.createTableIfMissing());
		assertFalse(config.createTableIgnoreErrors());
		assertNull(config.getConnectionPool());
		assertEquals(250, config.getExpiredQueryPageLimit());
	}
	

	@Test
	public void testConstructor_ignoreCreateTableErrors() {
		
		PersistenceConfigurationBuilder persistenceConfigurationBuilder = new ConfigurationBuilder().persistence();
		
		var b = new SQLStoreConfigurationBuilder(persistenceConfigurationBuilder)
			.createTableIgnoreErrors(true);
		
		SQLStoreConfiguration config = b.create();
		
		assertTrue(config.createTableIgnoreErrors());
	}


	@Test
	public void testExpiredQueryPageSize() {

		PersistenceConfigurationBuilder persistenceConfigurationBuilder = new ConfigurationBuilder().persistence();

		var b = new SQLStoreConfigurationBuilder(persistenceConfigurationBuilder)
			.expiredQueryPageLimit(100);

		SQLStoreConfiguration config = b.create();

		assertEquals(100, config.getExpiredQueryPageLimit());
	}


	@Test
	public void testExpiredQueryPageSize_illegal() {

		PersistenceConfigurationBuilder persistenceConfigurationBuilder = new ConfigurationBuilder().persistence();

		try {
			new SQLStoreConfigurationBuilder(persistenceConfigurationBuilder)
				.expiredQueryPageLimit(0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The expired query page limit must a positive integer", e.getMessage());
		}
	}
}
