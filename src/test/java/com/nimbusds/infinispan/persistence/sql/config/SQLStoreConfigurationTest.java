package com.nimbusds.infinispan.persistence.sql.config;


import java.util.Properties;

import static org.junit.Assert.assertEquals;

import org.infinispan.commons.util.StringPropertyReplacer;
import org.junit.Test;


public class SQLStoreConfigurationTest {
	
	
	@Test
	public void testNestedInterpolationNotSupported() {
		
		var props = new Properties();
		props.setProperty("catalina.home", "/opt/tomcat/");
		
		String out = StringPropertyReplacer.replaceProperties("${dataSource.url:h2:file:${catalina.home}/c2id}", props);
		
		// Nested props clearly don't work
		assertEquals("h2:file:${catalina.home/c2id}", out);
	}
	
	
	@Test
	public void testMultipleInterpolations() {
		
		var props = new Properties();
		props.setProperty("user", "c2id");
		props.setProperty("password", "secret");
		
		String out = StringPropertyReplacer.replaceProperties("jdbc:oracle:thin:${user}/${password}@//localhost:1521/XE", props);
		
		assertEquals("jdbc:oracle:thin:c2id/secret@//localhost:1521/XE", out);
	}
}
