package com.nimbusds.infinispan.persistence.sql;


import java.sql.Timestamp;
import java.util.*;

import static org.jooq.impl.DSL.field;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SQLDialect;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.sql.transformers.CustomSQLDataTypes;
import com.nimbusds.infinispan.persistence.sql.transformers.SQLFieldTransformer;


/**
 * Transforms User POJO to / from SQL record.
 */
public class UserRecordTransformer implements SQLRecordTransformer<String,User> {
	
	
	public static final String TABLE_NAME = "test_users";
	
	
	protected SQLDialect sqlDialect;
	
	
	private SQLFieldTransformer sqlFieldTransformer;
	
	
	@Override
	public void init(final InitParameters initParams) {
		sqlDialect = initParams.sqlDialect();
		sqlFieldTransformer = new SQLFieldTransformer(sqlDialect);
	}
	
	
	@Override
	public String getCreateTableStatement() {
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"id VARCHAR(255) PRIMARY KEY NOT NULL," +
				"given_name VARCHAR(255) NULL," +
				"surname VARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created TIMESTAMP NULL," +
				"permissions VARCHAR(255) ARRAY NULL" +
				")";
		}
		
		if (SQLDialect.MYSQL.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"id VARCHAR(255) NOT NULL," +
				"given_name VARCHAR(255) NULL," +
				"surname VARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created TIMESTAMP NULL," +
				"permissions JSON NULL," +
				"PRIMARY KEY (id)" +
				") CHARACTER SET = utf8";
		}
		
		if (SQLDialect.POSTGRES_9_5.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"id VARCHAR(255) NOT NULL," +
				"given_name VARCHAR(255) NULL," +
				"surname VARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created TIMESTAMPTZ NULL," +
				"permissions TEXT[] NULL," +
				"CONSTRAINT pk PRIMARY KEY (id)" +
				")";
		}
		
		if (SQLDialect.SQLSERVER2016.equals(sqlDialect)) {
			
			return "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='" + TABLE_NAME + "' and xtype='U') " +
				"CREATE TABLE " + TABLE_NAME + " (" +
				"id VARCHAR(255) NOT NULL," +
				"given_name NVARCHAR(255) NULL," +
				"surname NVARCHAR(255) NULL," +
				"email VARCHAR(255) NULL," +
				"created DATETIME NULL," +
				"permissions NVARCHAR(4000) NULL," +
				"CONSTRAINT pk PRIMARY KEY (id)" +
				")";
		}
		
		if (SQLDialect.ORACLE.equals(sqlDialect)) {
			
			return "CREATE TABLE " + TABLE_NAME + " (" +
				"id VARCHAR2(255) PRIMARY KEY," +
				"given_name NVARCHAR2(255)," +
				"surname NVARCHAR2(255)," +
				"email VARCHAR2(255)," +
				"created TIMESTAMP (0)," +
				"permissions NCLOB" +
				")";
		}
		
		throw new UnsupportedOperationException("Unsupported SQL dialect: " + sqlDialect);
	}
	
	
	@Override
	public String getTableName() {
		
		return TABLE_NAME;
	}
	
	
	@Override
	public Collection<Condition> resolveSelectionConditions(final String key) {
		
		return Collections.singleton(field("id").equal(key));
	}
	
	
	@Override
	public SQLRecord toSQLRecord(final InfinispanEntry<String,User> infinispanEntry) {
		
		Map<Field<?>,Object> fields = new LinkedHashMap<>();
		fields.put(field("id"), infinispanEntry.getKey());
		
		String givenNameString = infinispanEntry.getValue().getName().split("\\s")[0];
		if (SQLDialect.SQLSERVER.family().equals(sqlDialect.family())) {
			fields.put(field("given_name", CustomSQLDataTypes.SQLSERVER_NVARCHAR), givenNameString);
		} else {
			fields.put(field("given_name"), givenNameString);
		}
		
		String surnameString = infinispanEntry.getValue().getName().split("\\s")[1];
		if (SQLDialect.SQLSERVER.family().equals(sqlDialect.family())) {
			fields.put(field("surname", CustomSQLDataTypes.SQLSERVER_NVARCHAR), surnameString);
		} else {
			fields.put(field("surname"), surnameString);
		}
		
		fields.put(field("email"), infinispanEntry.getValue().getEmail());
		
		if (infinispanEntry.getValue().getCreated() != null) {
			fields.put(field("created"), new Timestamp(infinispanEntry.getValue().getCreated().toEpochMilli()));
		} else {
			fields.put(field("created"), null);
		}
		
		if (infinispanEntry.getValue().getPermissions() != null) {
			
			Object value = sqlFieldTransformer.toSQLCollection(infinispanEntry.getValue().getPermissions());
			
			fields.put(field("permissions"), value);
			
		} else {
			fields.put(field("permissions"), null);
		}
		
		return new ImmutableSQLRecord("id", fields);
	}
	
	
	@Override
	public InfinispanEntry<String,User> toInfinispanEntry(final RetrievedSQLRecord sqlRecord) {
		
		String id = sqlRecord.get("id", String.class);
		String givenName = sqlRecord.get("given_name", String.class);
		String surname = sqlRecord.get("surname", String.class);
		String name = givenName + " " + surname;
		String email = sqlRecord.get("email", String.class);
		
		Timestamp created = null;
		if (sqlRecord.get("created") != null) {
			created = sqlRecord.get("created", Timestamp.class);
		}
		
		List<String> permissions = null;
		if (sqlRecord.get("permissions") != null) {
			String[] permissionsArray = sqlFieldTransformer.parseSQLStringCollection("permissions", sqlRecord);
			if (permissionsArray != null) {
				permissions = Arrays.asList(permissionsArray);
			}
		}
		
		return new InfinispanEntry<>(
			id,
			new User(
				name,
				email,
				created != null ? created.toInstant() : null,
				permissions),
			null);
	}
}
