package com.nimbusds.infinispan.persistence.sql;


import com.codahale.metrics.Timer;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import net.jcip.annotations.NotThreadSafe;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.jooq.SQLDialect;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


@NotThreadSafe
@RunWith(Parameterized.class)
public class ExpiredEntryReaperWithExpConditionTest extends TestWithSQLDatabase {


	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}


	private SQLRecordTransformer<String,Session> entityTransformer;


	public ExpiredEntryReaperWithExpConditionTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
	}


	@Before
	public void setUp()
		throws Exception {

		super.setUp();

		entityTransformer = new SessionTransformer();
		entityTransformer.init(() -> sqlDialect);

		// Create table
		sql.execute(entityTransformer.getCreateTableStatement());

		// Insert expired
		InternalMetadata expMetadata = new InternalMetadataBuilder()
			.created(Instant.now().minus(61, ChronoUnit.MINUTES).toEpochMilli())
			.lifespan(60, TimeUnit.MINUTES)
			.build();

		for (int i=0; i < 10; ++i) {

			var key = "exp-" + i;
			var value = new Session("val-" + i, "{}");

			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>(key, value, expMetadata)).getFields())
				.execute();

			assertEquals(1, inserted);
		}

		// Insert active
		InternalMetadata actMetadata = new InternalMetadataBuilder()
			.created(Instant.now().toEpochMilli())
			.lifespan(60, TimeUnit.MINUTES)
			.build();

		for (int i=0; i < 150; ++i) {

			var key = "act-" + i;
			var value = new Session("val-" + i, "{}");

			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>(key, value, actMetadata)).getFields())
				.execute();

			assertEquals(1, inserted);
		}

		assertEquals(160, sql.fetchCount(table(entityTransformer.getTableName())));

		assertEquals(10, sql.selectFrom(table(entityTransformer.getTableName())).where(entityTransformer.getExpiredCondition(System.currentTimeMillis())).fetch().size());
	}
	
	
	@Test
	public void testExpireWithKeyListener() {

		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purgeWithKeyListener(
			key -> {
				assertTrue(key.startsWith("exp-"));
				deletedKeys.add(key);
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 10; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}
		
		assertEquals(10, deletedKeys.size());

		assertEquals(10, deleteTimer.getCount());

		assertEquals("Remaining non-expired", 150, sql.fetchCount(table(entityTransformer.getTableName())));
	}
	
	
	@Test
	public void testExpireWithEntryListener() {

		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purgeWithEntryListener(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<>() {
				
				@Override
				public void marshalledEntryPurged(MarshallableEntry<String, Session> entry) {
					assertTrue(entry.getKey().startsWith("exp-"));
					deletedKeys.add(entry.getKey());
					assertNotNull(entry.getValue());
					assertNotNull(entry.getMetadata());
				}
				
				
				@Override
				public void entryPurged(String s) {
					fail(); // never called
				}
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 10; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}
		
		assertEquals(10, deletedKeys.size());

		assertEquals("Remaining non-expired", 150, sql.fetchCount(table(entityTransformer.getTableName())));

		assertEquals(10, deleteTimer.getCount());
	}
}
