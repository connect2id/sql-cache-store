package com.nimbusds.infinispan.persistence.sql;


import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.jooq.impl.DSL.field;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SQLDialect;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;


/**
 * Transforms Group POJO to / from SQL record.
 */
public class GroupRecordTransformer implements SQLRecordTransformer<String,String> {
	
	
	private static final String TABLE_NAME = "test_groups";
	
	
	protected SQLDialect sqlDialect;
	
	
	@Override
	public void init(final InitParameters initParams) {
		sqlDialect = initParams.sqlDialect();
	}
	
	
	@Override
	public String getCreateTableStatement() {
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"name VARCHAR(255) PRIMARY KEY NOT NULL," +
				"email VARCHAR(255) NULL" +
				")";
		} 
		
		if (SQLDialect.MYSQL.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"name VARCHAR(255) NOT NULL," +
				"email VARCHAR(255) NULL" +
				") CHARACTER SET = utf8";
		}
		
		if (SQLDialect.POSTGRES_9_5.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"name VARCHAR(255) NOT NULL," +
				"email VARCHAR(255) NULL" +
				"CONSTRAINT pk PRIMARY KEY (name)" +
				")";
		}
		
		if (SQLDialect.SQLSERVER2016.equals(sqlDialect)) {
			
			return "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='" + TABLE_NAME + "' and xtype='U') " +
				"CREATE TABLE " + TABLE_NAME + " (" +
				"name VARCHAR(255) NOT NULL," +
				"email VARCHAR(255) NULL" +
				"CONSTRAINT pk PRIMARY KEY (id)" +
				")";
		}
		
		throw new UnsupportedOperationException("Unsupported SQL dialect: " + sqlDialect);
	}
	
	
	@Override
	public String getTableName() {
		
		return TABLE_NAME;
	}
	
	
	@Override
	public Collection<Condition> resolveSelectionConditions(final String key) {
		
		return Collections.singleton(field("name").equal(key));
	}
	
	
	@Override
	public SQLRecord toSQLRecord(final InfinispanEntry<String,String> infinispanEntry) {
		
		Map<Field<?>,Object> fields = new LinkedHashMap<>();
		fields.put(field("name"), infinispanEntry.getKey());
		fields.put(field("email"), infinispanEntry.getValue());
		return new ImmutableSQLRecord("name", fields);
	}
	
	
	@Override
	public InfinispanEntry<String,String> toInfinispanEntry(final RetrievedSQLRecord sqlRecord) {
		
		String name = sqlRecord.get("name", String.class);
		String email = sqlRecord.get("email", String.class);
		
		return new InfinispanEntry<>(name, email, null);
	}
}
