package com.nimbusds.infinispan.persistence.sql;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.jooq.SQLDialect;
import org.junit.Test;


public class SQLStoreTest {


	@Test
	public void supportedDBS() {
		
		assertTrue(SQLStore.SUPPORTED_DATABASES.contains(SQLDialect.H2));
		assertTrue(SQLStore.SUPPORTED_DATABASES.contains(SQLDialect.MYSQL));
		assertTrue(SQLStore.SUPPORTED_DATABASES.contains(SQLDialect.POSTGRES_9_5));
		assertTrue(SQLStore.SUPPORTED_DATABASES.contains(SQLDialect.SQLSERVER2016));
		assertTrue(SQLStore.SUPPORTED_DATABASES.contains(SQLDialect.ORACLE));
		assertEquals(5, SQLStore.SUPPORTED_DATABASES.size());
	}
}
