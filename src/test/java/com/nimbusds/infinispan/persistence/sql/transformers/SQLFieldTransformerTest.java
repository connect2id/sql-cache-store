package com.nimbusds.infinispan.persistence.sql.transformers;


import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.junit.Assert.*;

import net.minidev.json.JSONObject;
import org.infinispan.persistence.spi.PersistenceException;
import org.jooq.SQLDialect;
import org.junit.Test;

import com.nimbusds.infinispan.persistence.sql.TestWithSQLDatabase;


public class SQLFieldTransformerTest {
	
	
	// Transformer helpers
	@Test
	public void testObjectToString() {
		
		assertEquals("abc", SQLFieldTransformer.toString("abc"));
		
		assertEquals("ARRAY", SQLFieldTransformer.toString(CollectionDataType.ARRAY));
		
		assertEquals("https://c2id.com", SQLFieldTransformer.toString(URI.create("https://c2id.com")));
		
		assertNull(SQLFieldTransformer.toString(null));
	}


	@Test
	public void testObjectToUpperCaseString() {

		assertEquals("ABC", SQLFieldTransformer.toUpperCaseString("abc"));

		assertEquals("ARRAY", SQLFieldTransformer.toUpperCaseString(CollectionDataType.ARRAY));

		assertEquals("HTTPS://C2ID.COM", SQLFieldTransformer.toUpperCaseString(URI.create("https://c2id.com")));

		assertNull(SQLFieldTransformer.toUpperCaseString(null));
	}
	
	
	@Test
	public void testToSQLCollection_asJSONArray() {
		
		for (SQLDialect sqlDialect: List.of(SQLDialect.MYSQL, SQLDialect.SQLSERVER2016, SQLDialect.ORACLE)) {
			
			// No SQL array support, serialise to JSON array
			SQLFieldTransformer t = new SQLFieldTransformer(sqlDialect);
			
			assertEquals("[\"urn:a:b\",\"urn:c:d\"]", t.toSQLCollection(List.of(URI.create("urn:a:b"), URI.create("urn:c:d"))));
			
			assertNull(t.toSQLCollection(null));
		}
	}
	
	
	@Test
	public void testToSQLCollection_asStringArray() {
		
		// Has SQL array support
		for (SQLDialect sqlDialect: List.of(SQLDialect.H2, SQLDialect.POSTGRES_11, SQLDialect.POSTGRES_10, SQLDialect.POSTGRES_9_5, SQLDialect.POSTGRES_9_4, SQLDialect.POSTGRES_9_3)) {
			
			SQLFieldTransformer t = new SQLFieldTransformer(sqlDialect);
			
			assertArrayEquals(new String[]{"urn:a:b", "urn:c:d"}, (String[]) t.toSQLCollection(List.of(URI.create("urn:a:b"), URI.create("urn:c:d"))));
			
			assertNull(t.toSQLCollection(null));
		}
	}
	
	
	@Test
	public void testToSQLString_jsonObject() {
		
		var jsonObject = new JSONObject();
		jsonObject.put("a", 123);
		assertEquals("{\"a\":123}", SQLFieldTransformer.toSQLString(jsonObject));
		
		assertNull(SQLFieldTransformer.toSQLString(null));
	}
	
	
	@Test
	public void testToSQLTimestamp() {
		
		assertEquals(TestWithSQLDatabase.toSQLTimestamp(Instant.EPOCH), SQLFieldTransformer.toTimestamp(Instant.EPOCH).toString());
		
		Instant instant = Instant.ofEpochSecond(1473946131L);
		
		assertEquals(TestWithSQLDatabase.toSQLTimestamp(instant), SQLFieldTransformer.toTimestamp(instant).toString());
		
		assertNull(SQLFieldTransformer.toTimestamp(null));
	}
	
	
	// Parse helpers
	@Test
	public void testParseJSONObject() {
		
		JSONObject jsonObject = SQLFieldTransformer.parseJSONObject("{\"ip\":\"192.168.0.1\"}");
		assertEquals("192.168.0.1", jsonObject.get("ip"));
		assertEquals(1, jsonObject.size());
		
		assertNull(SQLFieldTransformer.parseJSONObject(null));
		
		try {
			SQLFieldTransformer.parseJSONObject("[]");
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getMessage().startsWith("Couldn't parse JSON object: "));
		}
	}
	
	
	@Test
	public void testParseStringArrayFromJSONArrayString_twoElements() {
	
		String s = "[\"openid\",\"email\"]";
		
		String[] values = SQLFieldTransformer.parseStringArrayFromJSONArrayString(s);
		
		assertEquals("openid", values[0]);
		assertEquals("email", values[1]);
		assertEquals(2, values.length);
	}
	
	
	@Test
	public void testParseStringArrayFromJSONArrayString_empty() {
	
		String s = "[]";
		
		String[] values = SQLFieldTransformer.parseStringArrayFromJSONArrayString(s);
		assertEquals(0, values.length);
	}
	
	
	@Test
	public void testParseStringArrayFromJSONArrayString_null() {

                assertNull(SQLFieldTransformer.parseStringArrayFromJSONArrayString(null));
	}
	
	
	// https://bitbucket.org/connect2id/server/issues/470/corrupted-scp-and-clm-in-authz-record-in
	@Test
	public void testParseStringArrayFromJSONArrayString_arrayOfJSONObjects() {
	
		String corruptValue = "[{}, {}, {}, {}, {}, {}, {}]";
		
		try {
			SQLFieldTransformer.parseStringArrayFromJSONArrayString(corruptValue);
			fail();
		} catch (PersistenceException e) {
			assertEquals("Couldn't parse JSON array of strings: Item 0 not a string: [{}, {}, {}, {}, {}, {}, {}]", e.getMessage());
		}
	}
	
	
	@Test
	public void testParseDate() {
		
		Instant now = Instant.ofEpochMilli(Instant.now().toEpochMilli());
		
		assertEquals(now, SQLFieldTransformer.parseInstant(new Timestamp(now.toEpochMilli())));
		
		assertNull(SQLFieldTransformer.parseInstant(null));
	}
}
