package com.nimbusds.infinispan.persistence.sql.config;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;


public class AttributeTest {
	

	@Test
	public void localNames() {
		
		assertNull(Attribute.UNKNOWN.getLocalName());
		assertEquals("record-transformer", Attribute.RECORD_TRANSFORMER.getLocalName());
		assertEquals("query-executor", Attribute.QUERY_EXECUTOR.getLocalName());
		assertEquals("sql-dialect", Attribute.SQL_DIALECT.getLocalName());
		assertEquals("create-table-if-missing", Attribute.CREATE_TABLE_IF_MISSING.getLocalName());
		assertEquals("create-table-ignore-errors", Attribute.CREATE_TABLE_IGNORE_ERRORS.getLocalName());
		assertEquals("connection-pool", Attribute.CONNECTION_POOL.getLocalName());
		assertEquals("expired-query-page-limit", Attribute.EXPIRED_QUERY_PAGE_LIMIT.getLocalName());
	}
}
