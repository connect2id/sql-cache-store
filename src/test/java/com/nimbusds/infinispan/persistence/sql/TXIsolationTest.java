package com.nimbusds.infinispan.persistence.sql;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.jcip.annotations.NotThreadSafe;
import org.jooq.SQLDialect;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;
import java.util.Properties;
import java.util.Set;

import static org.junit.Assert.assertEquals;


@NotThreadSafe
@RunWith(Parameterized.class)
public class TXIsolationTest extends TestWithSQLDatabase {


        @Parameterized.Parameters
        public static Collection<SQLDialect> input() {
                return SQLStore.SUPPORTED_DATABASES;
        }


        private HikariDataSource dataSource;


        public TXIsolationTest(final SQLDialect sqlDialect) {
                super(sqlDialect);
        }


        @Before
        @Override
        public void setUp()
                throws Exception {

                super.setUp();

                Properties props = getTestProperties(sqlDialect);

                Properties hikariProps = HikariConfigUtils.removeNonHikariProperties(props);
                hikariProps = HikariConfigUtils.removeBlankProperties(hikariProps);

                var hikariConfig = new HikariConfig(hikariProps);

                dataSource = new HikariDataSource(hikariConfig);
        }


        @Test
        public void testDefaultLevel() {

                Set<SQLDialect> readCommitted = Set.of(
                        SQLDialect.H2,
                        SQLDialect.POSTGRES_9_5,
                        SQLDialect.SQLSERVER2016,
                        SQLDialect.ORACLE
                );

                Set<SQLDialect> repeatableRead = Set.of(
                        SQLDialect.MYSQL
                );

                if (readCommitted.contains(sqlDialect)) {
                        assertEquals("TRANSACTION_READ_COMMITTED", TXIsolation.inspect(dataSource));
                } else if (repeatableRead.contains(sqlDialect)) {
                        assertEquals("TRANSACTION_REPEATABLE_READ", TXIsolation.inspect(dataSource));
                }
        }
}
