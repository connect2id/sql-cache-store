package com.nimbusds.infinispan.persistence.sql;


import com.codahale.metrics.Timer;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import net.jcip.annotations.NotThreadSafe;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.jooq.SQLDialect;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


@NotThreadSafe
@RunWith(Parameterized.class)
public class ExpiredEntryReaperTest extends TestWithSQLDatabase {
	
	
	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}
	
	
	public static class ExpiringUserRecordTransformer extends UserRecordTransformer {
		
		
		@Override
		public InfinispanEntry<String,User> toInfinispanEntry(final RetrievedSQLRecord sqlRecord) {
		
			InfinispanEntry<String,User> baseEntry = super.toInfinispanEntry(sqlRecord);

			Instant now = Instant.now();

			InternalMetadata metadata = null;
			if (baseEntry.getKey().startsWith("exp-")) {
				// Simulate expired entry
				metadata = new InternalMetadataBuilder()
					.created(Date.from(now.minus(24, ChronoUnit.HOURS)))
					.lastUsed(Date.from(now.minus(24, ChronoUnit.HOURS)))
					.lifespan(6L, TimeUnit.HOURS)
					.maxIdle(1L, TimeUnit.HOURS)
					.build();
				assertTrue(metadata.isExpired(now.toEpochMilli()));
			} else if (baseEntry.getKey().startsWith("act-")) {
				// Simulate active entry
				metadata = new InternalMetadataBuilder()
					.created(Date.from(now))
					.lastUsed(Date.from(now))
					.lifespan(6L, TimeUnit.HOURS)
					.maxIdle(1L, TimeUnit.HOURS)
					.build();
				assertFalse(metadata.isExpired(now.toEpochMilli()));
			} else if (baseEntry.getKey().startsWith("bad-")) {
				// Simulate bad entry
				throw new RuntimeException("Parse error: Illegal entry");
			} else {
				fail();
			}

			return new InfinispanEntry<>(baseEntry.getKey(), baseEntry.getValue(), metadata);
		}
	}
	
	
	private SQLRecordTransformer<String,User> entityTransformer;
	
	
	public ExpiredEntryReaperTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
	}
	
	
	@Before
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		entityTransformer = new ExpiringUserRecordTransformer();
		entityTransformer.init(() -> sqlDialect);
		
		// Create table
		sql.execute(entityTransformer.getCreateTableStatement());

		// Insert expired
		for (int i=0; i < 1000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>("exp-" + i, user, null)).getFields())
				.execute();
			
			assertEquals(1, inserted);
		}

		// Insert active
		for (int i=0; i < 500; ++i) {

			User user = new User("Alice " + i, "hello-" + i + "@email.com");

			int inserted = sql.insertInto(table(entityTransformer.getTableName()))
				.set(entityTransformer.toSQLRecord(new InfinispanEntry<>("act-" + i, user, null)).getFields())
				.execute();

			assertEquals(1, inserted);
		}
		
		assertEquals(1500, sql.fetchCount(table(entityTransformer.getTableName())));
	}


	@Test
	public void testConstants() {

		assertEquals(100, ExpiredEntryReaper.DELETE_BATCH_SIZE);
	}
	
	
	@Test
	public void testExpireWithKeyListener() {

		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purgeWithKeyListener(
			key -> {
				assertTrue(key.startsWith("exp-"));
				deletedKeys.add(key);
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 1000; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}
		
		assertEquals(1000, deletedKeys.size());

		assertEquals(1000, deleteTimer.getCount());

		assertEquals("Remaining non-expired", 500, sql.fetchCount(table(entityTransformer.getTableName())));
	}
	
	
	@Test
	public void testExpireWithEntryListener() {

		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), sql, entityTransformer, this::wrap, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		reaper.purgeWithEntryListener(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<>() {
				
				@Override
				public void marshalledEntryPurged(MarshallableEntry<String, User> entry) {
					assertTrue(entry.getKey().startsWith("exp-"));
					deletedKeys.add(entry.getKey());
					assertNotNull(entry.getValue());
					assertNotNull(entry.getMetadata());
				}
				
				
				@Override
				public void entryPurged(String s) {
					fail(); // never called
				}
			});
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 1000; ++i) {
			assertTrue(deletedKeys.contains("exp-" + i));
		}
		
		assertEquals(1000, deletedKeys.size());

		assertEquals("Remaining non-expired", 500, sql.fetchCount(table(entityTransformer.getTableName())));

		assertEquals(1000, deleteTimer.getCount());
	}
}
