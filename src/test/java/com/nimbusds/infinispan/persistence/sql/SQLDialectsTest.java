package com.nimbusds.infinispan.persistence.sql;


import org.jooq.Field;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;
import static org.junit.Assert.assertEquals;


/**
 * Verifies the output SQL query from JOOQ for H2, MySQL, PostgreSQL.
 */
public class SQLDialectsTest extends TestWithSQLDatabase {
	
	
	private static final Settings H2_SETTINGS;
	
	static {
		H2_SETTINGS = new Settings();
		H2_SETTINGS.setRenderNameStyle(RenderNameStyle.AS_IS);
	}
	
	
	public SQLDialectsTest() {
		super(SQLDialect.H2);
	}
	
	
	static Settings getSettings(final SQLDialect sqlDialect) {
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			return H2_SETTINGS;
		} else {
			return null;
		}
	}
	
	
	private String createContains(final SQLDialect sqlDialect) {
		
		return DSL.using(persistentConnection, sqlDialect, getSettings(sqlDialect))
			.selectOne()
			.from(table("users"))
			.where(field("id").equal("123"))
			.toString();
	}
	
	
	@Test
	public void testContains() {
		
		String expectedH2 =
			"select 1 one\n" +
			"from users\n" +
			"where id = '123'";
		
		assertEquals(expectedH2, createContains(SQLDialect.H2));
		
		
		String expectedMySQL =
			"select 1 as `one`\n" +
			"from users\n" +
			"where id = '123'";
		
		assertEquals(expectedMySQL, createContains(SQLDialect.MYSQL));
		
		
		String expectedP95 =
			"select 1 as \"one\"\n" +
			"from users\n" +
			"where id = '123'";
		
		assertEquals(expectedP95, createContains(SQLDialect.POSTGRES_9_5));
		
		
		String expectedSQLServer =
			"select 1 [one]\n" +
			"from users\n" +
			"where id = '123'";
		
		assertEquals(expectedSQLServer, createContains(SQLDialect.SQLSERVER2016));
		
		
		String expectedOracle =
			"select 1 \"one\"\n" +
			"from users\n" +
			"where id = '123'";
		
		assertEquals(expectedOracle, createContains(SQLDialect.ORACLE));
	}
	
	
	private String createLoad(final SQLDialect sqlDialect) {
		
		return DSL.using(persistentConnection, sqlDialect, getSettings(sqlDialect))
			.selectFrom(table("users"))
			.where(field("id").equal("123"))
			.toString();
	}
	
	
	@Test
	public void testLoad() {
		
		String expected = "select *\n" +
			"from users\n" +
			"where id = '123'";
		
		assertEquals(expected, createLoad(SQLDialect.H2));
		assertEquals(expected, createLoad(SQLDialect.MYSQL));
		assertEquals(expected, createLoad(SQLDialect.POSTGRES_9_5));
		assertEquals(expected, createLoad(SQLDialect.SQLSERVER2016));
		assertEquals(expected, createLoad(SQLDialect.ORACLE));
	}
	
	
	private String createDelete(final SQLDialect sqlDialect) {
		
		return DSL.using(persistentConnection, sqlDialect).deleteFrom(table("users")).where(field("id").equal("123")).toString();
	}
	
	
	@Test
	public void testDeleteOne() {
		
		String expected = "delete from users\n" +
			"where id = '123'";
		
		assertEquals(expected, createDelete(SQLDialect.H2));
		assertEquals(expected, createDelete(SQLDialect.MYSQL));
		assertEquals(expected, createDelete(SQLDialect.POSTGRES_9_5));
		assertEquals(expected, createDelete(SQLDialect.SQLSERVER2016));
		assertEquals(expected, createDelete(SQLDialect.ORACLE));
	}
	
	
	private String createUpsert(final SQLDialect sqlDialect) {
		
		Collection<Field<?>> cols = Arrays.asList(field("id"), field("name"), field("email"));
		Collection<Field<?>> keys = Collections.singleton(field("id"));
		
		return DSL.using(persistentConnection, sqlDialect, getSettings(sqlDialect))
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com")
			.toString();
	}
	
	
	@Test
	public void testUpsert() {
		
		String expectedH2 =
			"merge into users\n" +
			"(\n" +
			"  id,\n" +
			"  name,\n" +
			"  email\n" +
			")\n" +
			"key (id)\n" +
			"values ('123', 'alice', 'alice@email.com')";
		
		assertEquals(expectedH2, createUpsert(SQLDialect.H2));
		
		
		String expectedMySQL =
			"insert into users (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com'\n" +
			")\n" +
			"on duplicate key update\n" +
			"  id = '123',\n" +
			"  name = 'alice',\n" +
			"  email = 'alice@email.com'";
		
		assertEquals(expectedMySQL, createUpsert(SQLDialect.MYSQL));
		
		
		String expectedP95 =
			"insert into users (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com'\n" +
			")\n" +
			"on conflict (id)\n" +
			"do update\n" +
			"set\n" +
			"  id = '123',\n" +
			"  name = 'alice',\n" +
			"  email = 'alice@email.com'";
		
		assertEquals(expectedP95, createUpsert(SQLDialect.POSTGRES_9_5));
		
		
		String expectedSQLServer =
			"merge into users\n" +
			"using (\n" +
			"  select\n" +
			"    '123' [s1],\n" +
			"    'alice' [s2],\n" +
			"    'alice@email.com' [s3]\n" +
			") [src]\n" +
			"on id = [src].[s1]\n" +
			"when matched then update set\n" +
			"  name = [src].[s2],\n" +
			"  email = [src].[s3]\n" +
			"when not matched then insert (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email\n" +
			")\n" +
			"values (\n" +
			"  [src].[s1], \n" +
			"  [src].[s2], \n" +
			"  [src].[s3]\n" +
			");";
		
		assertEquals(expectedSQLServer, createUpsert(SQLDialect.SQLSERVER2016));
		
		
		String expectedOracle =
			"merge into users\n" +
			"using (\n" +
			"  select\n" +
			"    '123' \"s1\",\n" +
			"    'alice' \"s2\",\n" +
			"    'alice@email.com' \"s3\"\n" +
			"  from DUAL\n" +
			") \"src\"\n" +
			"on (id = \"src\".\"s1\")\n" +
			"when matched then update set\n" +
			"  name = \"src\".\"s2\",\n" +
			"  email = \"src\".\"s3\"\n" +
			"when not matched then insert (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email\n" +
			")\n" +
			"values (\n" +
			"  \"src\".\"s1\", \n" +
			"  \"src\".\"s2\", \n" +
			"  \"src\".\"s3\"\n" +
			")";
		
		assertEquals(expectedOracle, createUpsert(SQLDialect.ORACLE));
	}
	
	
	private String createUpsertWithStringListColumn(final SQLDialect sqlDialect) {
		
		Collection<Field<?>> cols = Arrays.asList(field("id"), field("name"), field("email"), field("permissions"));
		Collection<Field<?>> keys = Collections.singleton(field("id"));
		
		return DSL.using(persistentConnection, sqlDialect, getSettings(sqlDialect))
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", Arrays.asList("admin", "audit"))
			.toString();
	}
	
	
	private String createUpsertWithStringArrayColumn(final SQLDialect sqlDialect) {
		
		Collection<Field<?>> cols = Arrays.asList(field("id"), field("name"), field("email"), field("permissions"));
		Collection<Field<?>> keys = Collections.singleton(field("id"));
		
		return DSL.using(persistentConnection, sqlDialect, getSettings(sqlDialect))
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", "alice@email.com", new String[]{"admin", "audit"})
			.toString();
	}
	
	
	@Test
	public void testStringListColumn() {
		
		String expectedH2 =
			"merge into users\n" +
			"(\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"key (id)\n" +
			"values ('123', 'alice', 'alice@email.com', '[admin, audit]')";
		
		assertEquals(expectedH2, createUpsertWithStringListColumn(SQLDialect.H2));
		
		expectedH2 =
			"merge into users\n" +
			"(\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"key (id)\n" +
			"values ('123', 'alice', 'alice@email.com', array['admin', 'audit'])";
		
		assertEquals(expectedH2, createUpsertWithStringArrayColumn(SQLDialect.H2));
		
		String expectedMySQL =
			"insert into users (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  '[admin, audit]'\n" +
			")\n" +
			"on duplicate key update\n" +
			"  id = '123',\n" +
			"  name = 'alice',\n" +
			"  email = 'alice@email.com',\n" +
			"  permissions = '[admin, audit]'";
		
		assertEquals(expectedMySQL, createUpsertWithStringListColumn(SQLDialect.MYSQL));
		
		
		expectedMySQL =
			"insert into users (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  array['admin', 'audit']\n" +
			")\n" +
			"on duplicate key update\n" +
			"  id = '123',\n" +
			"  name = 'alice',\n" +
			"  email = 'alice@email.com',\n" +
			"  permissions = array['admin', 'audit']";
		
		assertEquals(expectedMySQL, createUpsertWithStringArrayColumn(SQLDialect.MYSQL));
		
		String expectedP95 =
			"insert into users (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  '[admin, audit]'\n" +
			")\n" +
			"on conflict (id)\n" +
			"do update\n" +
			"set\n" +
			"  id = '123',\n" +
			"  name = 'alice',\n" +
			"  email = 'alice@email.com',\n" +
			"  permissions = '[admin, audit]'";
		
		assertEquals(expectedP95, createUpsertWithStringListColumn(SQLDialect.POSTGRES_9_5));
		
		expectedP95 =
			"insert into users (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  'alice@email.com', \n" +
			"  cast('{\"admin\",\"audit\"}' as varchar[])\n" +
			")\n" +
			"on conflict (id)\n" +
			"do update\n" +
			"set\n" +
			"  id = '123',\n" +
			"  name = 'alice',\n" +
			"  email = 'alice@email.com',\n" +
			"  permissions = cast('{\"admin\",\"audit\"}' as varchar[])";
		
		assertEquals(expectedP95, createUpsertWithStringArrayColumn(SQLDialect.POSTGRES_9_5));
		
		String expectedSQLServer =
			"merge into users\n" +
			"using (\n" +
			"  select\n" +
			"    '123' [s1],\n" +
			"    'alice' [s2],\n" +
			"    'alice@email.com' [s3],\n" +
			"    '[admin, audit]' [s4]\n" +
			") [src]\n" +
			"on id = [src].[s1]\n" +
			"when matched then update set\n" +
			"  name = [src].[s2],\n" +
			"  email = [src].[s3],\n" +
			"  permissions = [src].[s4]\n" +
			"when not matched then insert (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  [src].[s1], \n" +
			"  [src].[s2], \n" +
			"  [src].[s3], \n" +
			"  [src].[s4]\n" +
			");";
		
		assertEquals(expectedSQLServer, createUpsertWithStringListColumn(SQLDialect.SQLSERVER2016));
		
		expectedSQLServer =
			"merge into users\n" +
			"using (\n" +
			"  select\n" +
			"    '123' [s1],\n" +
			"    'alice' [s2],\n" +
			"    'alice@email.com' [s3],\n" +
			"    array['admin', 'audit'] [s4]\n" +
			") [src]\n" +
			"on id = [src].[s1]\n" +
			"when matched then update set\n" +
			"  name = [src].[s2],\n" +
			"  email = [src].[s3],\n" +
			"  permissions = [src].[s4]\n" +
			"when not matched then insert (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  [src].[s1], \n" +
			"  [src].[s2], \n" +
			"  [src].[s3], \n" +
			"  [src].[s4]\n" +
			");";
		
		assertEquals(expectedSQLServer, createUpsertWithStringArrayColumn(SQLDialect.SQLSERVER2016));
		
		String expectedOracle =
			"merge into users\n" +
			"using (\n" +
			"  select\n" +
			"    '123' \"s1\",\n" +
			"    'alice' \"s2\",\n" +
			"    'alice@email.com' \"s3\",\n" +
			"    '[admin, audit]' \"s4\"\n" +
			"  from DUAL\n" +
			") \"src\"\n" +
			"on (id = \"src\".\"s1\")\n" +
			"when matched then update set\n" +
			"  name = \"src\".\"s2\",\n" +
			"  email = \"src\".\"s3\",\n" +
			"  permissions = \"src\".\"s4\"\n" +
			"when not matched then insert (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  \"src\".\"s1\", \n" +
			"  \"src\".\"s2\", \n" +
			"  \"src\".\"s3\", \n" +
			"  \"src\".\"s4\"\n" +
			")";
		
		assertEquals(expectedOracle, createUpsertWithStringListColumn(SQLDialect.ORACLE));
		
		expectedOracle =
			"merge into users\n" +
			"using (\n" +
			"  select\n" +
			"    '123' \"s1\",\n" +
			"    'alice' \"s2\",\n" +
			"    'alice@email.com' \"s3\",\n" +
			"    array['admin', 'audit'] \"s4\"\n" +
			"  from DUAL\n" +
			") \"src\"\n" +
			"on (id = \"src\".\"s1\")\n" +
			"when matched then update set\n" +
			"  name = \"src\".\"s2\",\n" +
			"  email = \"src\".\"s3\",\n" +
			"  permissions = \"src\".\"s4\"\n" +
			"when not matched then insert (\n" +
			"  id,\n" +
			"  name,\n" +
			"  email,\n" +
			"  permissions\n" +
			")\n" +
			"values (\n" +
			"  \"src\".\"s1\", \n" +
			"  \"src\".\"s2\", \n" +
			"  \"src\".\"s3\", \n" +
			"  \"src\".\"s4\"\n" +
			")";
		
		assertEquals(expectedOracle, createUpsertWithStringArrayColumn(SQLDialect.ORACLE));
	}
	
	
	private String upsertWithDateTime(final SQLDialect sqlDialect, final Timestamp date) {
		
		Collection<Field<?>> cols = Arrays.asList(field("uid"), field("name"), field("registered"));
		Collection<Field<?>> keys = Collections.singleton(field("uid"));

		if (SQLDialect.POSTGRES_9_5.equals(sqlDialect) || SQLDialect.ORACLE.equals(sqlDialect)) {

			return DSL.using(persistentConnection, sqlDialect, getSettings(sqlDialect))
				.insertInto(table("users"))
				.columns(cols)
				.values("123", "alice", date.toString())
				.onConflict(field("uid"))
				.doUpdate()
				.set(field("name"), "alice")
				.set(field("registered"), date.toString())
				.toString();
		}

		return DSL.using(persistentConnection,sqlDialect, getSettings(sqlDialect))
			.mergeInto(table("users"), cols)
			.key(keys)
			.values("123", "alice", date.toString())
			.toString();
	}
	
	
	@Test
	public void testDateTime() {
		
		var date = new Timestamp(1474006010900L);
		
		String expectedH2 =
			"merge into users\n" +
			"(\n" +
			"  uid,\n" +
			"  name,\n" +
			"  registered\n" +
			")\n" +
			"key (uid)\n" +
			"values ('123', 'alice', '" + toSQLTimestamp(date.toInstant()) + "')";
		
		assertEquals(expectedH2, upsertWithDateTime(SQLDialect.H2, date));
		
		String expectedMySQL =
			"insert into users (\n" +
			"  uid,\n" +
			"  name,\n" +
			"  registered\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  '2016-09-16 09:06:50.9'\n" +
			")\n" +
			"on duplicate key update\n" +
			"  uid = '123',\n" +
			"  name = 'alice',\n" +
			"  registered = '" + toSQLTimestamp(date.toInstant()) + "'";
		
		assertEquals(expectedMySQL, upsertWithDateTime(SQLDialect.MYSQL, date));
		
		String expectedP95 =
			"insert into users (\n" +
			"  uid,\n" +
			"  name,\n" +
			"  registered\n" +
			")\n" +
			"values (\n" +
			"  '123', \n" +
			"  'alice', \n" +
			"  '" + toSQLTimestamp(date.toInstant()) + "'\n" +
			")\n" +
			"on conflict (uid)\n" +
			"do update\n" +
			"set\n" +
			"  name = 'alice',\n" +
			"  registered = '" + toSQLTimestamp(date.toInstant()) + "'";
		
		assertEquals(expectedP95, upsertWithDateTime(SQLDialect.POSTGRES_9_5, date));
		
		String expectedSQLServer =
			"merge into users\n" +
			"using (\n" +
			"  select\n" +
			"    '123' [s1],\n" +
			"    'alice' [s2],\n" +
			"    '" + toSQLTimestamp(date.toInstant()) + "' [s3]\n" +
			") [src]\n" +
			"on uid = [src].[s1]\n" +
			"when matched then update set\n" +
			"  name = [src].[s2],\n" +
			"  registered = [src].[s3]\n" +
			"when not matched then insert (\n" +
			"  uid,\n" +
			"  name,\n" +
			"  registered\n" +
			")\n" +
			"values (\n" +
			"  [src].[s1], \n" +
			"  [src].[s2], \n" +
			"  [src].[s3]\n" +
			");";
		
		assertEquals(expectedSQLServer, upsertWithDateTime(SQLDialect.SQLSERVER2016, date));
		
		String expectedOracle =
			"merge into users\n" +
			"using (\n" +
			"  (\n" +
			"    select\n" +
			"      '123' \"uid\",\n" +
			"      'alice' \"name\",\n" +
			"      '" + toSQLTimestamp(date.toInstant()) + "' \"registered\"\n" +
			"    from DUAL\n" +
			"  )\n" +
			") \"t\"\n" +
			"on ((\n" +
			"  (\n" +
			"    uid,\n" +
			"    1\n" +
			"  ) = ((\"t\".\"uid\", 1))\n" +
			"  or 1 = 0\n" +
			"))\n" +
			"when matched then update set\n" +
			"  name = 'alice',\n" +
			"  registered = '" + toSQLTimestamp(date.toInstant()) + "'\n" +
			"when not matched then insert (\n" +
			"  uid,\n" +
			"  name,\n" +
			"  registered\n" +
			")\n" +
			"values (\n" +
			"  \"t\".\"uid\", \n" +
			"  \"t\".\"name\", \n" +
			"  \"t\".\"registered\"\n" +
			")";
		
		assertEquals(expectedOracle, upsertWithDateTime(SQLDialect.ORACLE, date));
	}
}
