package com.nimbusds.infinispan.persistence.sql;


import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.field;


/**
 * Transforms User POJO to / from SQL record.
 */
public class SessionTransformer implements SQLRecordTransformer<String,Session> {
	
	
	public static final String TABLE_NAME = "test_sessions";
	
	
	protected SQLDialect sqlDialect;
	
	
	@Override
	public void init(final InitParameters initParams) {
		sqlDialect = initParams.sqlDialect();
	}
	
	
	@Override
	public String getCreateTableStatement() {
		
		if (SQLDialect.H2.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"id VARCHAR(255) PRIMARY KEY NOT NULL," +
				"name VARCHAR(255) NOT NULL," +
				"t_created TIMESTAMP NOT NULL," +
				"max_life INT NOT NULL," +
				"data VARCHAR(1000)" +
				")";
		}
		
		if (SQLDialect.MYSQL.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"id VARCHAR(255) NOT NULL," +
				"name VARCHAR(255) NOT NULL," +
				"t_created TIMESTAMP NOT NULL," +
				"max_life INT NOT NULL," +
				"data JSON," +
				"PRIMARY KEY (id)" +
				") CHARACTER SET = utf8";
		}
		
		if (SQLDialect.POSTGRES_9_5.equals(sqlDialect)) {
			
			return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
				"id VARCHAR(255) NOT NULL," +
				"name VARCHAR(255) NOT NULL," +
				"t_created TIMESTAMPTZ NOT NULL," +
				"max_life INT NOT NULL," +
				"data JSON," +
				"CONSTRAINT pk_session PRIMARY KEY (id)" +
				")";
		}
		
		if (SQLDialect.SQLSERVER2016.equals(sqlDialect)) {
			
			return "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='" + TABLE_NAME + "' and xtype='U') " +
				"CREATE TABLE " + TABLE_NAME + " (" +
				"id VARCHAR(255) NOT NULL," +
				"name NVARCHAR(255) NOT NULL," +
				"t_created DATETIME NOT NULL," +
				"max_life INT NOT NULL," +
				"data NVARCHAR(1000)," +
				"CONSTRAINT pk_session PRIMARY KEY (id)" +
				")";
		}
		
		if (SQLDialect.ORACLE.equals(sqlDialect)) {
			
			return "CREATE TABLE " + TABLE_NAME + " (" +
				"id VARCHAR2(255) PRIMARY KEY," +
				"name NVARCHAR2(255) NOT NULL," +
				"t_created TIMESTAMP (0) NOT NULL," +
				"max_life INT NOT NULL," +
				"data NCLOB" +
				")";
		}
		
		throw new UnsupportedOperationException("Unsupported SQL dialect: " + sqlDialect);
	}
	
	
	@Override
	public String getTableName() {
		
		return TABLE_NAME;
	}
	
	
	@Override
	public Collection<Condition> resolveSelectionConditions(final String key) {
		
		return Collections.singleton(field("id").equal(key));
	}


	@Override
	public Collection<OrderField<?>> getKeyColumnsForExpiredEntryReaper() {
		return Collections.singletonList(field("id"));
	}


	@Override
	public List<Object> getKeyValuesForExpiredEntryReaper(final String key) {
		return Collections.singletonList(key);
	}


	@Override
	public Condition getExpiredCondition(final long now) {
		Condition hasMaxLife = field("max_life", Long.class).greaterThan(DSL.inline(-1L));
		Condition maxLifeExpired =
			DSL.timestampAdd(field("t_created", Timestamp.class), field("max_life", Long.class), DatePart.SECOND)
			.lessOrEqual(new Timestamp(now));
		return DSL.and(hasMaxLife, maxLifeExpired);
	}


	@Override
	public SQLRecord toSQLRecord(final InfinispanEntry<String,Session> infinispanEntry) {
		
		Map<Field<?>,Object> fields = new LinkedHashMap<>();
		fields.put(field("id"), infinispanEntry.getKey());
		fields.put(field("name"), infinispanEntry.getValue().getName());
		fields.put(field("data"), JSON.json(infinispanEntry.getValue().getData()));
		fields.put(field("t_created"), new Timestamp(infinispanEntry.getMetadata().created()));
		fields.put(field("max_life"), infinispanEntry.getMetadata().lifespan() / 1000); // seconds

		return new ImmutableSQLRecord("id", fields);
	}
	
	
	@Override
	public InfinispanEntry<String,Session> toInfinispanEntry(final RetrievedSQLRecord sqlRecord) {
		
		String id = sqlRecord.get("id", String.class);
		String name = sqlRecord.get("name", String.class);
		String data = sqlRecord.get("data", String.class);
		Timestamp created = sqlRecord.get("t_created", Timestamp.class);
		long maxLife = sqlRecord.get("max_life", Long.class);

		return new InfinispanEntry<>(
			id,
			new Session(name, data),
			new InternalMetadataBuilder()
				.created(created.getTime())
				.lifespan(maxLife, TimeUnit.SECONDS)
				.build());
	}
}
