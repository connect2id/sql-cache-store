package com.nimbusds.infinispan.persistence.sql;


import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.common.InfinispanStore;
import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import com.nimbusds.infinispan.persistence.common.util.InfinispanUtils;
import com.nimbusds.infinispan.persistence.sql.config.SQLStoreConfigurationBuilder;
import com.nimbusds.infinispan.persistence.sql.transformers.SQLFieldTransformer;
import net.jcip.annotations.NotThreadSafe;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.context.Flag;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.infinispan.persistence.spi.PersistenceException;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


/**
 * Tests the SQL store with a programmatic config.
 */
@NotThreadSafe
@RunWith(Parameterized.class)
public class SQLStoreWithProgConfigTest extends TestWithSQLDatabase {
	
	
	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return SQLStore.SUPPORTED_DATABASES;
	}


	private static final String CACHE_NAME = "userMap";


	private EmbeddedCacheManager cacheMgr;
	
	
	private final SQLFieldTransformer fieldTransformer;
	
	
	private static Connection getSQLConnection()
		throws SQLException {
		
		return ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getDataSource().getConnection();
	}
	
	
	public SQLStoreWithProgConfigTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
		fieldTransformer = new SQLFieldTransformer(sqlDialect);
	}
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {

		super.setUp();
		
		Properties props = getTestProperties(sqlDialect);

		cacheMgr = new DefaultCacheManager();

		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(SQLStoreConfigurationBuilder.class)
			.segmented(false)
			.recordTransformerClass(UserRecordTransformer.class)
			.queryExecutorClass(UserQueryExecutor.class)
			.sqlDialect(sqlDialect)
			.createTableIfMissing(true) // optional, defaults to true
			.withProperties(props)
			.create();

		b.memory()
			.maxCount(100)
			.whenFull(EvictionStrategy.REMOVE)
			.create();

		b.expiration()
			.wakeUpInterval(50L, TimeUnit.MILLISECONDS)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		// Shut down Infinispan first
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		// Shut down DB
		super.tearDown();
	}
	
	
	@Test
	public void testGetSQLRecordTransformer() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		SQLRecordTransformer<String, User> tr = ((SQLStore)SQLStore.getInstances().get(CACHE_NAME)).getSQLRecordTransformer();
		
		assertEquals(new UserRecordTransformer().getTableName(), tr.getTableName());
	}
	
	
	@Test
	public void testSimpleObjectLifeCycle()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Get non-existing object
		assertNull(myMap.get("invalid-key"));
		
		// Store new object with all fields defined
		Instant created = Instant.ofEpochMilli(1474012953000L);
		var u1 = new User("Alice Adams", "alice@wonderland.net", created, Arrays.asList("admin", "audit"));
		assertNull(myMap.putIfAbsent("u1", u1));
		
		// Check presence
		assertTrue(myMap.containsKey("u1"));
		
		
		// Confirm SQL insert
		RetrievedSQLRecord r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Alice",  r.get("given_name"));
		assertEquals("Adams",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(new String[]{"admin", "audit"}, fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
	
		
		// Get new count
		assertEquals(1, myMap.size());
		
		// Get object
		User out = myMap.get("u1");
		assertEquals(u1.getName(), out.getName());
		assertEquals(u1.getEmail(), out.getEmail());
		assertEquals(u1.getCreated(), out.getCreated());
		assertEquals(u1.getPermissions(), out.getPermissions());
		
		// Update object
		var u2 = new User("Bob Brown", "bob@wonderland.net", created, Arrays.asList("browse", "pay"));
		assertNotNull(myMap.replace("u1", u2));
		
		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Bob",  r.get("given_name"));
		assertEquals("Brown",  r.get("surname"));
		assertEquals("bob@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(new String[]{"browse", "pay"}, fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u2.getName(), out.getName());
		assertEquals(u2.getEmail(), out.getEmail());
		assertEquals(u2.getCreated(), out.getCreated());
		assertEquals(u2.getPermissions(), out.getPermissions());
		
		// Update object with fewer defined fields
		var u3 = new User("Claire Cox", "claire@wonderland.net");
		assertNotNull(myMap.replace("u1", u3));
		
		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Claire",  r.get("given_name"));
		assertEquals("Cox",  r.get("surname"));
		assertEquals("claire@wonderland.net",  r.get("email"));
		assertNull(r.get("created")); // Timestamp
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		
		// Remove object
		out = myMap.remove("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		// Confirm removal
		assertNull(myMap.get("u1"));
		assertFalse(myMap.containsKey("u1"));
		
		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		}
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}


	@Test
	public void testOracle_putIfAbsent() {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		if (! SQLDialect.ORACLE.equals(sqlDialect)) {
			return; // break
		}

		Instant created = Instant.ofEpochMilli(1474012953000L);
		var u1 = new User("Alice Adams", "alice@wonderland.net", created, List.of("read", "write"));

		assertNull(myMap.putIfAbsent("u1", u1));
	}
	
	
	@Test
	public void testOracle_largePermissionsCLOB()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		if (! SQLDialect.ORACLE.equals(sqlDialect)) {
			return; // break
		}
		
		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Store new object with all fields defined
		
		List<String> permissions = new LinkedList<>();
		for (int i=0; i< 2000; i++) {
			permissions.add(UUID.randomUUID().toString());
		}
		
		Instant created = Instant.ofEpochMilli(1474012953000L);
		var u1 = new User("Alice Adams", "alice@wonderland.net", created, permissions);
		assertNull(myMap.putIfAbsent("u1", u1));
		
		// Check presence
		assertTrue(myMap.containsKey("u1"));
		
		
		// Confirm SQL insert
		RetrievedSQLRecord r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Alice",  r.get("given_name"));
		assertEquals("Adams",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(permissions.toArray(), fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
	
		
		// Get new count
		assertEquals(1, myMap.size());
		
		// Get object
		User out = myMap.get("u1");
		assertEquals(u1.getName(), out.getName());
		assertEquals(u1.getEmail(), out.getEmail());
		assertEquals(u1.getCreated(), out.getCreated());
		assertEquals(u1.getPermissions(), out.getPermissions());
		
		// Update object
		permissions = new LinkedList<>();
		for (int i=0; i< 3000; i++) {
			permissions.add(UUID.randomUUID().toString());
		}
		var u2 = new User("Bob Brown", "bob@wonderland.net", created, permissions);
		assertNotNull(myMap.replace("u1", u2));
		
		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Bob",  r.get("given_name"));
		assertEquals("Brown",  r.get("surname"));
		assertEquals("bob@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(permissions.toArray(), fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u2.getName(), out.getName());
		assertEquals(u2.getEmail(), out.getEmail());
		assertEquals(u2.getCreated(), out.getCreated());
		assertEquals(u2.getPermissions(), out.getPermissions());
		
		// Update object with fewer defined fields
		var u3 = new User("Claire Cox", "claire@wonderland.net");
		assertNotNull(myMap.replace("u1", u3));
		
		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Claire",  r.get("given_name"));
		assertEquals("Cox",  r.get("surname"));
		assertEquals("claire@wonderland.net",  r.get("email"));
		assertNull(r.get("created")); // Timestamp
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		
		// Remove object
		out = myMap.remove("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		// Confirm removal
		assertNull(myMap.get("u1"));
		assertFalse(myMap.containsKey("u1"));
		
		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		}
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}


	@Test
	public void testOracle_largePermissionsCLOB_withQuoteChar()
		throws Exception {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		if (! SQLDialect.ORACLE.equals(sqlDialect)) {
			return; // break
		}

		assertNotNull(SQLStore.getInstances().get(CACHE_NAME));
		assertEquals(1, SQLStore.getInstances().size());

		// Initial size
		assertEquals(0, myMap.size());

		// Store new object with all fields defined

		List<String> permissions = new LinkedList<>();
		for (int i=0; i< 2000; i++) {
			permissions.add(UUID.randomUUID() + "'"); // Add quote char
		}

		Instant created = Instant.ofEpochMilli(1474012953000L);
		var u1 = new User("Alice Adams", "alice@wonderland.net", created, permissions);
		assertNull(myMap.putIfAbsent("u1", u1));

		// Check presence
		assertTrue(myMap.containsKey("u1"));


		// Confirm SQL insert
		RetrievedSQLRecord r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Alice",  r.get("given_name"));
		assertEquals("Adams",  r.get("surname"));
		assertEquals("alice@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(permissions.toArray(), fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());


		// Get new count
		assertEquals(1, myMap.size());

		// Get object
		User out = myMap.get("u1");
		assertEquals(u1.getName(), out.getName());
		assertEquals(u1.getEmail(), out.getEmail());
		assertEquals(u1.getCreated(), out.getCreated());
		assertEquals(u1.getPermissions(), out.getPermissions());

		// Update object
		permissions = new LinkedList<>();
		for (int i=0; i< 3000; i++) {
			permissions.add(UUID.randomUUID().toString());
		}
		var u2 = new User("Bob Brown", "bob@wonderland.net", created, permissions);
		assertNotNull(myMap.replace("u1", u2));

		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Bob",  r.get("given_name"));
		assertEquals("Brown",  r.get("surname"));
		assertEquals("bob@wonderland.net",  r.get("email"));
		assertEquals(created, r.get("created", Timestamp.class).toInstant());
		assertArrayEquals(permissions.toArray(), fieldTransformer.parseSQLStringCollection("permissions", r));
		assertEquals(6, r.size());

		// Get object
		out = myMap.get("u1");
		assertEquals(u2.getName(), out.getName());
		assertEquals(u2.getEmail(), out.getEmail());
		assertEquals(u2.getCreated(), out.getCreated());
		assertEquals(u2.getPermissions(), out.getPermissions());

		// Update object with fewer defined fields
		var u3 = new User("Claire Cox", "claire@wonderland.net");
		assertNotNull(myMap.replace("u1", u3));

		// Confirm SQL update
		r = wrap(sql.select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		assertEquals("u1", r.get("id"));
		assertEquals("Claire",  r.get("given_name"));
		assertEquals("Cox",  r.get("surname"));
		assertEquals("claire@wonderland.net",  r.get("email"));
		assertNull(r.get("created")); // Timestamp
		assertNull(r.get("permissions"));
		assertEquals(6, r.size());

		// Get object
		out = myMap.get("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());


		// Remove object
		out = myMap.remove("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());

		// Confirm removal
		assertNull(myMap.get("u1"));
		assertFalse(myMap.containsKey("u1"));

		// Confirm SQL delete
		try (Connection c = getSQLConnection()) {
			assertNull(DSL.using(c).select().from(table("test_users")).where(field("id").eq("u1")).fetchOne());
		}

		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}
	
	
	@Test
	public void testMultiplePutIfAbsentWithEviction() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(100, myMap.getCacheConfiguration().memory().maxCount());
		assertEquals(EvictionStrategy.REMOVE, myMap.getCacheConfiguration().memory().whenFull());
		assertEquals(50, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		
		String[] keys = new String[200];
		
		final Instant created = Instant.ofEpochMilli(1474102000L);
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			var value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, Arrays.asList("admin", "audit"));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Confirm SQL insert
		for (int i=0; i < 200; i ++) {
			
			RetrievedSQLRecord r = wrap(sql.select().from(table("test_users")).where(field("id").eq(keys[i])).fetchOne());
			assertEquals(keys[i], r.get("id"));
			assertEquals("Alice",  r.get("given_name"));
			assertEquals("Adams-" + i,  r.get("surname"));
			assertEquals("alice-"+i+"@email.com",  r.get("email"));
			assertEquals(created, r.get("created", Timestamp.class).toInstant());
			assertArrayEquals(new String[]{"admin", "audit"}, fieldTransformer.parseSQLStringCollection("permissions", r));
			assertEquals(6, r.size());
		}
		
		// Calls AdvancedCacheLoader.process, result matches eviction size
		assertEquals(100, myMap.getAdvancedCache().getDataContainer().size());
		
		// Calls AdvancedCacheLoader.process, result equals total persisted
		assertEquals(200, myMap.size());
		
		// Check presence
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		assertEquals(200, myMap.size());
		
		// Evict entries from memory, store unaffected
		for (String uid: keys) {
			myMap.evict(uid);
		}
		
		assertEquals(200, myMap.size());
		
		// Recheck presence, loads data from LDAP
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		// Confirm SQL presence
		for (int i=0; i < 200; i ++) {
			
			RetrievedSQLRecord r = wrap(sql.select().from(table("test_users")).where(field("id").eq(keys[i])).fetchOne());
			assertEquals(keys[i], r.get("id"));
			assertEquals("Alice",  r.get("given_name"));
			assertEquals("Adams-" + i,  r.get("surname"));
			assertEquals("alice-"+i+"@email.com",  r.get("email"));
			assertEquals(created, r.get("created", Timestamp.class).toInstant());
			assertArrayEquals(new String[]{"admin", "audit"}, fieldTransformer.parseSQLStringCollection("permissions", r));
			assertEquals(6, r.size());
		}
		
		// Evict entries from memory, store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}
		
		assertEquals(200, myMap.size());
		
		// Iterate and count, calls AdvancedCacheLoader.process
		var counter = new AtomicInteger();
		myMap.keySet().iterator().forEachRemaining(uuid -> counter.incrementAndGet());
		assertEquals(200, counter.get());
		
		// Remove from cache and store
		for (int i=0; i < 200; i ++) {
			assertNotNull(myMap.remove(keys[i]));
		}
		
		assertEquals(0, myMap.size());
		
		// Confirm SQL deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(sql.select().from(table("test_users")).where(field("id").eq(keys[i])).fetchOne());
		}
		
		// Confirm deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(myMap.get(keys[0]));
		}
	}
	
	
	@Test
	public void testPutWithMissingTable() throws InterruptedException {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		dropTables();
		
		Thread.sleep(100);
		
		try {
			myMap.put("alice", new User("Alice Adams", "alice@name.com"));
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getMessage().startsWith("SQL"));
		}
	}
	
	
	@Test
	public void testPutAsyncWithMissingTable()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		dropTables();
		
		// No exception thrown here, instead in future
		CompletableFuture<User> future = myMap.putAsync("alice", new User("Alice Adams", "alice@name.com"));
		
		while (! future.isDone()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}
		
		try {
			future.get();
			fail();
		} catch (ExecutionException e) {
			var persistenceException = (PersistenceException)e.getCause();
			assertTrue(persistenceException.getMessage().startsWith("SQL"));
		}
		
		try {
			myMap.size();
			fail();
		} catch (Exception e) {
			// ok
		}
		
		assertEquals(0, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
	}
	
	
	@Test
	public void testKeyFilterProcess() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		// Initial state empty
		Map<String,User> filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertTrue(filteredMap.isEmpty());
		
		// Put entry
		String k1 = "alice";
		var u1 = new User("Alice Adams", "alice@name.com");
		myMap.put(k1, u1);
		
		// Confirm put
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		// Filter all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(1, filteredMap.size());
		
		// Put another entry
		String k2 = "bob";
		var u2 = new User("Bob Brown", "bob@name.com");
		myMap.put(k2, u2);
		
		// Filter on name
		filteredMap = myMap
			.entrySet()
			.stream()
			.filter(uuidUserEntry -> uuidUserEntry.getValue().getName().equals("Bob Brown"))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(1, filteredMap.size());
		
		// Filter to include all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(2, filteredMap.size());
	}
	
	
	@Test
	public void testRepeatPut() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "alice";
		var u1 = new User("Alice Adams", "alice@name.com");
		
		// First put
		myMap.put(k1, u1);
		
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		// Repeat put
		myMap.put(k1, u1);
		
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
	}
	
	
	@Test
	public void testPutExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "alice";
		var u1 = new User("Alice Adams", "alice@name.com");
		
		myMap.put(k1, u1, 60L, TimeUnit.HOURS);
		
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		myMap.put(k1, u1, 60L, TimeUnit.HOURS);
	}
	
	
	@Test
	public void testExists() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "alice";
		var u1 = new User("Alice Adams", "alice@name.com");
		
		myMap.put(k1, u1);
		
		assertTrue(SQLStore.getInstances().get(CACHE_NAME).contains(k1));
		
		assertFalse(SQLStore.getInstances().get(CACHE_NAME).contains("no-such-key"));
	}
	
	
	@Test
	public void testPurgeNonExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			var value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Nothing to purge with regular user records
		
		SQLStore.getInstances().get(CACHE_NAME).purge(Executors.newSingleThreadExecutor(), new AdvancedCacheExpirationWriter.ExpirationPurgeListener() {
			
			@Override
			public void marshalledEntryPurged(MarshallableEntry entry) {
				fail();
			}
			
			
			@Override
			public void entryPurged(Object o) {
				fail();
			}
		});
		
		assertEquals(200, myMap.size());
	}
	
	
	@Test
	public void testSize() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(0, SQLStore.getInstances().get(CACHE_NAME).size());
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			var value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		assertEquals(200, SQLStore.getInstances().get(CACHE_NAME).size());
		
		myMap.clear();
		
		assertEquals(0, SQLStore.getInstances().get(CACHE_NAME).size());
	}
	
	
	@Test
	public void testGetKeysWithColdCache() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		Set<String> keys = new HashSet<>();
		for (int i=0; i < 10; i++) {
			String k = "u" + i;
			keys.add(k);
			var u = new User("Alice " + i, "alice" + i + "@name.com");
			myMap.put(k, u);
		}
		
		assertEquals(keys, myMap.keySet());
		
		myMap.getAdvancedCache().getDataContainer().clear();
		
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
		
		assertEquals(keys, myMap.keySet());
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void testDirectSQLQueryExecution() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		QueryExecutor<String,User> queryExecutor = SQLStore.getInstances().get(CACHE_NAME).getQueryExecutor();
		
		// Expect empty result set
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> fail("Unexpected result"));
		
		assertTrue(myMap.isEmpty());
		
		
		myMap.put("a", new User("Alice Adams", "alice@wonderland.net"));
		
		
		List<User> matches = new ArrayList<>();
		
		// Expect 1 result
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Alice Adams", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		assertEquals(1, matches.size());
		
		myMap.put("b", new User("Bob Brown", "bob@wonderland.net"));
		
		// Expect 1 result
		matches.clear();
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Alice Adams", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		assertEquals(1, matches.size());
		
		
		myMap.put("c", new User("Claire Cox", "alice@wonderland.net"));
		
		// Expect 2 results
		matches.clear();
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Alice Adams", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		
		assertEquals("Claire Cox", matches.get(1).getName());
		assertEquals("alice@wonderland.net", matches.get(1).getEmail());
		assertEquals(2, matches.size());
		
		myMap.remove("a");
		
		// Expect 1 result
		matches.clear();
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Claire Cox", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		assertEquals(1, matches.size());
		
		
		myMap.clear();
		
		// Expect zero results
		queryExecutor.executeQuery(
			new SimpleMatchQuery<>("email", "alice@wonderland.net"),
			infinispanEntry -> fail("Unexpected result"));
	}
	
	
	@Test
	public void testDirectWriteWithNullMetadata() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		InfinispanStore<String, User> infinispanStore = SQLStore.getInstances().get(CACHE_NAME);
		
		var key ="a";
		var value = new User("Alice Adams", "alice@wonderland.net");
		
		infinispanStore.write(InfinispanUtils.toMarshallableEntry(key, value));
		
		assertEquals(value, myMap.get(key));
	}
	
	
	@Test
	public void testMetrics() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		MetricRegistry metricRegistry = MonitorRegistries.getMetricRegistry();
		
		Map<String,Timer> timers = metricRegistry.getTimers();
		assertTrue(timers.containsKey(CACHE_NAME+".sqlStore.pool.Wait"));
		assertTrue(timers.containsKey(CACHE_NAME+".sqlStore.loadTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".sqlStore.writeTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".sqlStore.deleteTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".sqlStore.processTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".sqlStore.purgeTimer"));
		
		Map<String,Histogram> histograms = metricRegistry.getHistograms();
		assertTrue(histograms.containsKey(CACHE_NAME+".sqlStore.pool.Usage"));
		assertTrue(histograms.containsKey(CACHE_NAME+".sqlStore.pool.ConnectionCreation"));
		assertEquals(2, histograms.size());
		
		Map<String,Gauge> gauges = metricRegistry.getGauges();
		assertTrue(gauges.containsKey(CACHE_NAME+".sqlStore.pool.TotalConnections"));
		assertTrue(gauges.containsKey(CACHE_NAME+".sqlStore.pool.IdleConnections"));
		assertTrue(gauges.containsKey(CACHE_NAME+".sqlStore.pool.ActiveConnections"));
		assertTrue(gauges.containsKey(CACHE_NAME+".sqlStore.pool.PendingConnections"));
		assertTrue(gauges.containsKey(CACHE_NAME+".sqlStore.pool.MinConnections"));
		assertTrue(gauges.containsKey(CACHE_NAME+".sqlStore.pool.MaxConnections"));
		assertEquals(6, gauges.size());
	}
}