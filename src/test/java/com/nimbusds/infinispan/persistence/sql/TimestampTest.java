package com.nimbusds.infinispan.persistence.sql;

import net.jcip.annotations.NotThreadSafe;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.sql.Timestamp;
import java.util.Collection;

import static org.jooq.impl.DSL.field;


@NotThreadSafe
@RunWith(Parameterized.class)
public class TimestampTest extends TestWithSQLDatabase {


        @Parameterized.Parameters
        public static Collection<SQLDialect> input() {
                return SQLStore.SUPPORTED_DATABASES;
        }


        public TimestampTest(final SQLDialect sqlDialect) {
                super(sqlDialect);
        }


        @Test
        public void testCompare() {

                Result<Record1<Timestamp>> result = sql.select(DSL.currentTimestamp()).fetch();

                Timestamp currentTimestamp = result.get(0).get(field("current_timestamp"), Timestamp.class);
                System.out.println(currentTimestamp);
                System.out.println(new Timestamp(System.currentTimeMillis()));
        }
}
