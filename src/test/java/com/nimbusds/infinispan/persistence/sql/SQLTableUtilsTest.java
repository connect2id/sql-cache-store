package com.nimbusds.infinispan.persistence.sql;


import net.jcip.annotations.NotThreadSafe;
import org.jooq.SQLDialect;
import org.jooq.exception.DataAccessException;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.jooq.impl.DSL.table;
import static org.junit.Assert.*;


@NotThreadSafe
@RunWith(Parameterized.class)
public class SQLTableUtilsTest extends TestWithSQLDatabase {


	@Parameterized.Parameters
	public static Collection<SQLDialect> input() {
		return List.of(SQLDialect.H2, SQLDialect.ORACLE);
	}
	
	
	public SQLTableUtilsTest(final SQLDialect sqlDialect) {
		super(sqlDialect);
	}


	@After
	@Override
	public void tearDown() throws Exception {
		dropTable("zero_col_table");
		dropTable("one_col_table");
		dropTable("two_col_table");
		super.tearDown();
	}

	@Test
	public void testInvalidTable(){
		
		try {
			SQLTableUtils.getColumnNames(table("no-such-table"), sql);
			fail();
		} catch (DataAccessException e) {
			assertTrue(e.getMessage().startsWith("SQL"));
		}
	}
	
	
	@Test
	public void testGetColumnNames_zero() {

		if (SQLDialect.ORACLE.equals(sqlDialect)) {
			// Zero column tables not supported
			return;
		}

		sql.execute("CREATE TABLE zero_col_table");
		
		List<String> colsNames = SQLTableUtils.getColumnNames(table("zero_col_table"), sql);
		
		assertTrue(colsNames.isEmpty());
	}
	
	
	@Test
	public void testGetColumnNames_one() {
		
		sql.execute("CREATE TABLE one_col_table (name VARCHAR(100))");
		
		List<String> colsNames = SQLTableUtils.getColumnNames(table("one_col_table"), sql);
		
		assertEquals(Collections.singletonList("name"), colsNames);
	}
	
	
	@Test
	public void testGetColumnNames_two() {
		
		sql.execute("CREATE TABLE two_col_table (name VARCHAR(100), email VARCHAR(100))");
		
		List<String> colsNames = SQLTableUtils.getColumnNames(table("two_col_table"), sql);
		
		assertEquals("name", colsNames.get(0));
		assertEquals("email", colsNames.get(1));
		assertEquals(2, colsNames.size());
	}
}
